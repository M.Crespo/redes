#include <iostream>
#include <cassert>
#include <cstdio>
#include <unistd.h>
#include <string>
#include <vector>
#include <sstream>
#include <cstring>
#include <exception>


#include "../shared/UDPSocket.hpp"
#include "../shared/UDPServer.hpp"
#include "../shared/helperFunctions.hpp"
#include "pdAPI.hpp"

#define MAX(a,b) (a > b ?  a : b)

std::string pdIP;
std::string PDport = "57062";
std::string ASIP = "127.0.0.1";
std::string ASport = "58062";
std::string UID;
std::string password;

UDPSocket* sock;
UDPServer* server;

void showUsage(){
    std::string out = std::string("./pd PDIP [-d PDport] [-n ASIP] [-p ASport]\n");
    write(1, out.c_str(), out.length());
    exit(EXIT_FAILURE);
}

bool isInputValid(int argc, char** argv){
    if(argc < 2 || argc > 8) 
    {
        return false;
    }

    if(!redesHelperFunctions::isValidIP(argv[1])) {
        return false;
    }
    
    //flags always come in pairs of flag -> value
    if(argc%2 != 0){
        return false;
    }
    return true;
}

void processFlags(int argc, char** argv){

    for(int i = 2; i < argc; i+=2){
        if(!strcmp(argv[i], "-d")){
            PDport = std::string(argv[i+1]);
        }
        else if(!strcmp(argv[i], "-n")){
            if(!redesHelperFunctions::isValidIP(argv[i+1])){
                showUsage();
                exit(1);
            }
            ASIP = std::string(argv[i+1]);
        }
        else if(!strcmp(argv[i], "-p")){
            ASport = std::string(argv[i+1]);
        }
    }
}

int processInputArgs(int argc, char** argv) {
    if(!isInputValid(argc, argv)){
        showUsage();
        exit(1);
    }
    
    pdIP = std::string(argv[1]);

    if(argc > 2){
        processFlags(argc, argv);
    }
    return 0;
}

void parseStdIn(UDPSocket* socket){
    std::string commandID;
    
    char inputString[128];

    if(fgets(inputString, 128, stdin) == nullptr) {
        perror("Error getting the input from the user.");
        exit(EXIT_FAILURE);
    }
    
    std::string command;

    command = std::string(inputString);

    std::vector<std::string> components = redesHelperFunctions::getCommandComponents(command);
    commandID = components[0];
    if(commandID == "reg"){
        UID = components[1];
        password = components[2];
        pdAPI::registerUser(UID, password, pdIP, PDport, socket);
    }
    else if(commandID == "exit"){
        pdAPI::exitPD(UID, password, socket);
        throw redesHelperFunctions::ExitSucessException();
    }
    else{
        std::string outStr = "Command not defined\n";
        write(1, outStr.c_str(), outStr.length());
    }
}

void parseASMessage(UDPServer* server){
    std::string message;
    std::string commandID;

    server->readPayload(message);
    std::vector<std::string> components = redesHelperFunctions::getCommandComponents(message);
    
    commandID = components[0];
    if(commandID == "VLC"){
        pdAPI::respondToVLC(message, UID, server);
        std::vector<std::string> messageComponents = redesHelperFunctions::getCommandComponents(message);
        std::cout << "------- New Validation Code --------" << std::endl;
        std::cout << "Validation Code: " + messageComponents[2] << std::endl;
        std::cout << "Operation: " + messageComponents[3] << std::endl;
        if (messageComponents[3] != "L" && messageComponents[3] != "X"){
            std::cout << "Filename: " + messageComponents[4] << std::endl;
        }
    }

    return;
}

int main(int argc, char** argv){
    fd_set rfds;

    if(int output = processInputArgs(argc, argv)){
        exit(output);
    }
    
    auto sock =  new UDPSocket(ASIP, ASport, AF_INET, 128);
    auto server = new UDPServer(pdIP,PDport, AF_INET, 128);
    
    while(1){
        FD_ZERO(&rfds);
        FD_SET(0, &rfds);
        FD_SET(server->getFileDescriptor(), &rfds);
        int selectReturn = select(server->getFileDescriptor() + 1, 
        &rfds, nullptr, nullptr, nullptr);
        
        if(selectReturn == -1){
            perror("select() Error!\n");
            exit(EXIT_FAILURE);
        }
        else if(selectReturn){
            try{
                if(FD_ISSET(0, &rfds)){
                    parseStdIn(sock);
                }
            
                if(FD_ISSET(server->getFileDescriptor(), &rfds)){
                    parseASMessage(server);
                }
            }
            catch(const redesHelperFunctions::ExitSucessException&){
                //Break out of the infinite loop of server coms, so we can orderly exit the server

                //Maybe do the same for exit failure? idk
                break;
            }
        }
    }

    delete server;
    delete sock;
    return 0;
}
