#ifndef PDAPI_HPP
#define PDAPI_HPP

#include <iostream>
#include <string>
#include <sstream>
#include "../shared/UDPSocket.hpp"
#include "../shared/UDPServer.hpp"




namespace pdAPI {
    std::string registerUser(const std::string& uid, const std::string& password, const std::string& pdIP, const std::string& pdPort, UDPSocket* sock);
    std::string exitPD(const std::string& uid, const std::string& password, UDPSocket* socket);
    std::string respondToVLC(std::string& message, const std::string& UID, UDPServer* server);
}



#endif