#include "pdAPI.hpp"


std::string pdAPI::registerUser(const std::string& UID,
    const std::string& password,
    const std::string& pdIP,
    const std::string& pdPort,
    UDPSocket* sock){

    std::string command;
    //REG UID pass PDIP PDport
    command = "REG " +  UID + " " + password + " "+ pdIP + " " + pdPort + "\n";
    
    sock->sendPayload(command);

    std::string response;

    sock->readPayload(response);

    if(response == "RRG OK\n"){
        std::cout << "Registration Successful." << std::endl;
    }
    else if(response == "RRG NOK\n"){
        std::cout << "Registration failed." << std::endl;
    }
    else if(response == "ERR\n"){
        std::cout << "ERR: Wrong Protocol." << std::endl;
    }
    return response;
}

std::string pdAPI::exitPD(const std::string& uid, const std::string& password, UDPSocket* socket){
    std::string payload = "UNR " + uid + " " + password + "\n";
    std::string ASresponse;

    socket->sendPayload(payload);

    socket->readPayload(ASresponse);

    socket->closeConnections();
    
    return ASresponse;
}

std::string pdAPI::respondToVLC(std::string& message, const std::string& UID, UDPServer* server){
    std::string command, UIDOnMessage, response;

    std::istringstream f(message);

    getline(f, command, ' ');
    getline(f, UIDOnMessage, ' ');
    
    response = "RVC " + UID;

    (UIDOnMessage == UID) ? response += " OK\n" : response += " NOK\n";

    server->sendPayload(response);
    return response;
}