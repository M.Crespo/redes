#include <iostream>
#include <map>
#include <vector>


#include "../shared/TCPSocket.hpp"
#include "../shared/UDPSocket.hpp"
#include "../shared/helperFunctions.hpp"
#include "../shared/TCPServer.hpp"
#include "../shared/UDPServer.hpp"
#include "checkCommands.hpp"
#include "processCommands.hpp"

#define MAX(a, b) (a > b ? a : b)

std::string FSIP = "127.0.0.1";
std::string ASIP = "127.0.0.1";
std::string ASPort = "58062";
std::string FSPort = "59062";
bool verbose = false;

std::map<int, redesHelperFunctions::fdInfo> fdMap;


bool isInputValid(int argc, char** argv){
    if(argc > 8) 
    {
        return false;
    }
    return true;
}

void checkActiveClients()
{
    //skip UDP and TCP Servers
    for (auto it = fdMap.begin(); it != fdMap.end();)
    {
        if (it->second.isActive)
        {
            it->second.isActive = false;
            continue;
        }
        else
        {
            //TODO: delete file from a given file descriptor in fpmap
            close(it->first);
            fdMap.erase(it++);
            continue;
        }
        it++;
    }
}

void showUsage (){
    std::cout << "./FS [-q FSport] [-n ASIP] [-p ASport] [-v]" << std::endl;
}

bool processFlags(int argc, char** argv){

    for(int i = 1; i < argc; i+=2){
        if(!strcmp(argv[i], "-n")){
            if(!redesHelperFunctions::isValidIP(argv[i+1])){
                showUsage();
                return false;
            }
            ASIP = std::string(argv[i+1]);
        }
        else if(!strcmp(argv[i], "-p")){
            ASPort = std::string(argv[i+1]);
        }
        else if(!strcmp(argv[i], "-q")){
            FSPort = std::string(argv[i+1]);
        }
        else if(!strcmp(argv[i], "-v")){
            verbose = true;
        }
    }
            return true;

}

bool processInput(int argc, char** argv) {
    if(!isInputValid(argc, argv)){
        showUsage();
        return false;
    }

    if(argc > 1){
        return processFlags(argc, argv);
    }
    return true;
}

int parseTCPMessage(TCPServer *tcpServer, UDPServer* udpServer,  int fd){
    std::string message;
    auto addrInfoUDP = udpServer->getAddressInfo();

    std::string tcpIp = fdMap.at(fd).ip;
    std::string tcpPort = fdMap.at(fd).port;

    int n = tcpServer->readPayload(fd, message);
    auto components = redesHelperFunctions::getCommandComponents(message);
    std::string commandID = components[0];

    UDPSocket* asUdpSocket = new UDPSocket(ASIP,ASPort,AF_INET, 128);

    if (commandID == "LST"){
        if (checkLSTCommand(message)){
            
            processLSTCommand(tcpServer, asUdpSocket, fd, tcpIp, tcpPort, components, verbose);
            if(verbose){
                std::cout << "----- List Command Successful (LST) -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        } else {
            tcpServer->sendPayload(fd, "RLS ERR\n");
            if (verbose){
                std::cout << "----- List Command Failed -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        }
    }
    else if (commandID == "RTV"){
        if (checkRTVCommand(message)){
            processRTVCommand(tcpServer, asUdpSocket, fd, tcpIp, tcpPort, components, verbose);
            if(verbose){
                std::cout << "----- Retrieve Command Successful -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        } else {
            tcpServer->sendPayload(fd, "RRT ERR\n");
            if (verbose){
                std::cout << "----- Retrieve Command Failed -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        }
    }
    else if (commandID == "UPL"){
        if (checkUPLCommand(message)){
            processUPLCommand(tcpServer, asUdpSocket, fd, tcpIp, tcpPort, components, verbose);
            if(verbose){
                std::cout << "----- Upload Command Successful -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        } else {
            tcpServer->sendPayload(fd, "RUP ERR\n");
            if (verbose){
                std::cout << "----- Upload Command Failed -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        }
    } else if (commandID == "DEL"){
        if (checkDELCommand(message)){
            processDELCommand(tcpServer, asUdpSocket, fd, tcpIp, tcpPort, components, verbose);
            if(verbose){
                std::cout << "----- Delete Command Successful -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        } else {
            tcpServer->sendPayload(fd, "RDL ERR\n");
            if (verbose){
                std::cout << "----- Delete Command Failed -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        }
    } else if (commandID == "REM"){
        if (checkREMCommand(message)){
            processREMCommand(tcpServer, asUdpSocket, fd, tcpIp, tcpPort, components, verbose);
            if(verbose){
                std::cout << "----- Remove Command Successful -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        } else {
            tcpServer->sendPayload(fd, "RRM ERR\n");
            if (verbose){
                std::cout << "----- Remove Command Failed -----" << std::endl;
                std::cout << "UID: " << components[1] << std::endl;
                std::cout << "TID: " << components[2] << std::endl;
                std::cout << "IP: " << tcpIp << std::endl;
                std::cout << "Port: " << tcpPort << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        }
    }
    delete asUdpSocket;
    return n;
}

int main(int argc, char** argv){
    fd_set rfds;
    int biggestFD;
    verbose = false;
    if (!processInput(argc, argv))
    {
        exit(EXIT_FAILURE);
    }

    auto tcpServer = new TCPServer(FSPort, 128);
    auto udpServer = new UDPServer(FSPort, 128);
    biggestFD = udpServer->getFileDescriptor();

    while (1)
    {
        FD_ZERO(&rfds);
        struct timeval tv = {20, 0};
        //Set the fds again
        FD_SET(tcpServer->getFileDescriptor(), &rfds);
        FD_SET(udpServer->getFileDescriptor(), &rfds);

        for (auto it = fdMap.begin(); it != fdMap.end(); it++)
        {
            FD_SET(it->first, &rfds);
        }

        int selectReturn = select(biggestFD + 1, &rfds, nullptr, nullptr, &tv);

        if (selectReturn == -1)
        {
            perror("Error on pselect.");
            exit(EXIT_FAILURE);
        }
        else if (selectReturn == 0)
        {
            checkActiveClients();
        }
        else if (selectReturn)
        {
            try
            {
                //New Connection
                if (FD_ISSET(tcpServer->getFileDescriptor(), &rfds))
                {
                    redesHelperFunctions::fdInfo fdInfo;
                    int fd = tcpServer->tcpAccept(fdInfo.ip, fdInfo.port);
                    fdMap.insert({fd, fdInfo});
                    biggestFD = MAX(biggestFD, fd);
                }
                //New TCP message
                for (auto it = fdMap.begin(); it != fdMap.end();)
                {
                    if (FD_ISSET(it->first, &rfds))
                    {
                        if (parseTCPMessage(tcpServer, udpServer, it->first))
                        {
                            it->second.isActive = true;
                        }
                        else
                        {
                            close(it->first);
                            fdMap.erase(it++);
                            continue;
                        }
                    }
                    it++;
                }
            }
            catch (const redesHelperFunctions::ExitSucessException &)
            {
                //Break out of the infinite loop of server coms, so we can orderly exit the server

                //Maybe do the same for exit failure?
                break;
            }
        }
    }

    return 0;
}
