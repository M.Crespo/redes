#include "checkCommands.hpp"

bool checkLSTCommand(const std::string &command){
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);

    return (*((command.end()) - 1) == '\n') &&
           (commandComponents.size() == 3) &&
           (commandComponents[0] == "LST") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 4) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[2]));
}

bool checkRTVCommand(const std::string &command){
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);
    //TODO: ADD FILE NAME CHECK
    return (*((command.end()) - 1) == '\n') &&
           (commandComponents.size() == 4) &&
           (commandComponents[0] == "RTV") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 4) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[2]));
}

bool checkUPLCommand(const std::string &command){
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);
    //TODO: ADD FILE NAME CHECK
    return (commandComponents[0] == "UPL") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 4) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[2])) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[4]));
}

bool checkDELCommand(const std::string &command){
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);
    //TODO: ADD FILE NAME CHECK
    return (*((command.end()) - 1) == '\n') &&
           (commandComponents.size() == 4) &&
           (commandComponents[0] == "DEL") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 4) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[2]));
}

bool checkREMCommand(const std::string &command){
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);
    return (*((command.end()) - 1) == '\n') &&
           (commandComponents.size() == 3) &&
           (commandComponents[0] == "REM") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 4) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[2]));
}