#include "processCommands.hpp"

bool checkTID(UDPSocket *udpSocket,std::string UID, std::string TID){
    std::string payload = "VLD " + UID + " " + TID + "\n";
    udpSocket->sendPayload(payload);
    std::string response;
    udpSocket->readPayload(response);
    // CNF UID TID Fop [Fname]
    std::vector<std::string> responseComponents = redesHelperFunctions::getCommandComponents(response);

    return responseComponents[3] != "E";
}

void processLSTCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose){
    std::string UID = components[1];
    std::string TID = components[2];
    if (checkTID(udpSocket, UID, TID)){
        std::string dirname = "files/" + UID + "/";
        std::vector<std::string> filenames;
        redesHelperFunctions::ListDir(dirname, filenames);
        std::string payload = "RLS " + std::to_string(filenames.size());
        if (filenames.size() == 0){
            tcpServer->sendPayload(fd,"RLS EOF\n");
            if (verbose){
                std::cout << "----- List Command Failed (EOF)-----" << std::endl;
                std::cout << "UID: " << UID << std::endl;
                std::cout << "TID: " << TID << std::endl;
                std::cout << "IP: " << ip << std::endl;
                std::cout << "Port: " << port << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
            return;
        }
        for (std::string filename : filenames){
            payload +=" " + filename + " " + std::to_string(redesHelperFunctions::getFileSize(dirname + filename));
        }
        payload += "\n";
        tcpServer->sendPayload(fd,payload);
        if (verbose){
            std::cout << "----- List Command Successful-----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "TID: " << TID << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
    } else {
            if (verbose){
                std::cout << "----- List Command Failed (INV)-----" << std::endl;
                std::cout << "UID: " << UID << std::endl;
                std::cout << "TID: " << TID << std::endl;
                std::cout << "IP: " << ip << std::endl;
                std::cout << "Port: " << port << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        tcpServer->sendPayload(fd,"RLS INV\n");    
        return;
    }
}


void processRTVCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose){
    std::string UID = components[1];
    std::string TID = components[2];
    std::string fileName = components[3];
    if (checkTID(udpSocket, UID, TID)){
        std::string dirname = "files/" + UID + "/";

        if(redesHelperFunctions::checkIfDirectoryExists(dirname)){
            if(redesHelperFunctions::fileExists(dirname + fileName)){
                std::vector<std::string> fileLines;
                std::string fileContent = redesHelperFunctions::readFromFileToString(dirname+fileName);
                int fileSize = redesHelperFunctions::getFileSize(dirname+fileName);
                
                //TODO: if wrong protocol, \n is wrong
                std::string payload = "RRT OK " + std::to_string(fileSize) + " ";
                tcpServer->sendPayload(fd, payload);
                
                redesHelperFunctions::readFromFileToSocket(fd, fileSize, dirname+fileName);

                tcpServer->sendPayload(fd, "\n");

                if (verbose){
                    std::cout << "----- Retrieve Command Successful -----" << std::endl;
                    std::cout << "UID: " << UID << std::endl;
                    std::cout << "TID: " << TID << std::endl;
                    std::cout << "IP: " << ip << std::endl;
                    std::cout << "Port: " << port << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                }
                return;
            }else{
                tcpServer->sendPayload(fd,"RRT EOF\n");
                if (verbose){
                    std::cout << "----- Retrieve Command Failed (EOF) -----" << std::endl;
                    std::cout << "UID: " << UID << std::endl;
                    std::cout << "TID: " << TID << std::endl;
                    std::cout << "IP: " << ip << std::endl;
                    std::cout << "Port: " << port << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                }
            }
        }
        else{
            tcpServer->sendPayload(fd,"RRT NOK\n");
            if (verbose){
                std::cout << "----- Retrieve Command Failed (NOK) -----" << std::endl;
                std::cout << "UID: " << UID << std::endl;
                std::cout << "TID: " << TID << std::endl;
                std::cout << "IP: " << ip << std::endl;
                std::cout << "Port: " << port << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
        }
           
    } else {
        if (verbose){
            std::cout << "----- Retrieve Command Failed (INV) -----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "TID: " << TID << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
        tcpServer->sendPayload(fd,"RRT INV\n");    
        return;
    }
}

void processUPLCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose){
    std::string UID = components[1];
    std::string TID = components[2];
    std::string fileName = components[3];
    std::string Fsize = components[4];
    if (checkTID(udpSocket, UID, TID)){
        std::string dirname = "files/" + UID + "/";

        if(!redesHelperFunctions::checkIfDirectoryExists(dirname)){
            //TODO: Add this to a "redesHelperFunctions" function
            mkdir(dirname.data(), 0700);
        }
        else {
            std::vector<std::string> files;
            redesHelperFunctions::ListDir(dirname, files);
            if (std::count(files.begin(), files.end(), fileName) != 0){
                //file already exists
                tcpServer->sendPayload(fd, "RUP DUP\n");
                if (verbose){
                    std::cout << "----- Upload Command Failed (DUP) -----" << std::endl;
                    std::cout << "UID: " << UID << std::endl;
                    std::cout << "TID: " << TID << std::endl;
                    std::cout << "IP: " << ip << std::endl;
                    std::cout << "Port: " << port << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                }   
                return;
            }
            if (files.size() >= 15){
                //server is full
                tcpServer->sendPayload(fd, "RUP FULL\n");
                if (verbose){
                    std::cout << "----- Upload Command Failed (FULL) -----" << std::endl;
                    std::cout << "UID: " << UID << std::endl;
                    std::cout << "TID: " << TID << std::endl;
                    std::cout << "IP: " << ip << std::endl;
                    std::cout << "Port: " << port << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                } 
                return;
            }
        }

        redesHelperFunctions::readFromSocketToFile(fd, stoi(Fsize), dirname + fileName);

        tcpServer->sendPayload(fd, "RUP OK\n");
        if (verbose){
            std::cout << "----- Upload Command Successful -----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "TID: " << TID << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        } 
           
    } else {
        tcpServer->sendPayload(fd,"RUP INV\n");   
        if (verbose){
            std::cout << "----- Upload Command Failed (INV) -----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "TID: " << TID << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        } 
        return;
    }
}

void processDELCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose){
    std::string UID = components[1];
    std::string TID = components[2];
    std::string fileName = components[3];

    if (checkTID(udpSocket, UID, TID)){
        std::string dirname = "files/" + UID + "/";

        if(redesHelperFunctions::checkIfDirectoryExists(dirname)){
            if(redesHelperFunctions::fileExists(dirname + fileName)){
                redesHelperFunctions::deleteFile(dirname + fileName);
                tcpServer->sendPayload(fd, "RDL OK\n");
                if (verbose){
                    std::cout << "----- Delete Command Successful -----" << std::endl;
                    std::cout << "UID: " << UID << std::endl;
                    std::cout << "TID: " << TID << std::endl;
                    std::cout << "IP: " << ip << std::endl;
                    std::cout << "Port: " << port << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                }   
            }else{
                tcpServer->sendPayload(fd,"RDL EOF\n");
                if (verbose){
                    std::cout << "----- Delete Command Failed (EOF) -----" << std::endl;
                    std::cout << "UID: " << UID << std::endl;
                    std::cout << "TID: " << TID << std::endl;
                    std::cout << "IP: " << ip << std::endl;
                    std::cout << "Port: " << port << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                } 
            }
        }
        else{
            tcpServer->sendPayload(fd,"RDL NOK\n");
            if (verbose){
                std::cout << "----- Delete Command Failed (NOK) -----" << std::endl;
                std::cout << "UID: " << UID << std::endl;
                std::cout << "TID: " << TID << std::endl;
                std::cout << "IP: " << ip << std::endl;
                std::cout << "Port: " << port << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            } 
        }
           
    } else {
        if (verbose){
            std::cout << "----- Delete Command Failed (INV) -----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "TID: " << TID << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        } 
        tcpServer->sendPayload(fd,"RDL INV\n");    
        return;
    }
}

void processREMCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose){
    std::string UID = components[1];
    std::string TID = components[2];

    if (checkTID(udpSocket, UID, TID)){
        std::string dirname = "files/" + UID + "/";

        if(redesHelperFunctions::checkIfDirectoryExists(dirname)){
            redesHelperFunctions::deleteDirectory(dirname);
            tcpServer->sendPayload(fd, "RRM OK\n");
            if (verbose){
                std::cout << "----- Delete Command Successful -----" << std::endl;
                std::cout << "UID: " << UID << std::endl;
                std::cout << "TID: " << TID << std::endl;
                std::cout << "IP: " << ip << std::endl;
                std::cout << "Port: " << port << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            } 
        }
        else{
            tcpServer->sendPayload(fd,"RRM NOK\n");
            if (verbose){
                std::cout << "----- Remove Command Failed (NOK) -----" << std::endl;
                std::cout << "UID: " << UID << std::endl;
                std::cout << "TID: " << TID << std::endl;
                std::cout << "IP: " << ip << std::endl;
                std::cout << "Port: " << port << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            } 
        }
           
    } else {
        if (verbose){
            std::cout << "----- Remove Command Failed (INV) -----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "TID: " << TID << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        } 
        tcpServer->sendPayload(fd,"RDL INV\n");    
        return;
    }
}