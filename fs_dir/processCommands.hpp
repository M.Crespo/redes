#ifndef PROCESSCOMMANDSFS_HPP
#define PROCESSCOMMANDSFS_HPP

#include <iostream>
#include <map>
#include <time.h>
#include <experimental/filesystem>

#include "../shared/helperFunctions.hpp"
#include "../shared/UDPSocket.hpp"
#include "../shared/TCPServer.hpp"

bool checkTID(UDPSocket *udpSocket,std::string UID, std::string TID);
void processLSTCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose);
void processRTVCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose);
void processUPLCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose);
void processDELCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose);
void processREMCommand(TCPServer *tcpServer, UDPSocket *udpSocket, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose);




#endif