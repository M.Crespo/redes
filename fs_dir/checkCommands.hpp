#ifndef CHECKCOMMANDSFS_HPP
#define CHECKCOMMANDSFS_HPP
#include <iostream>

#include "../shared/helperFunctions.hpp"

bool checkLSTCommand(const std::string &command);
bool checkRTVCommand(const std::string &command);
bool checkUPLCommand(const std::string &command);
bool checkDELCommand(const std::string &command);
bool checkREMCommand(const std::string &command);
#endif