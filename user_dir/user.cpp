#include <iostream>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include <vector>
#include <sstream>
#include <cstring>
#include <exception>
#include <ctime>

#include "../shared/TCPSocket.hpp"
#include "../shared/helperFunctions.hpp"
#include "userAPI.hpp"


std::string ASIP = "127.0.0.1";
std::string ASport = "58062";
std::string FSIP = "127.0.0.1";
std::string FSPort = "59062";

int RID;
std::string TID;
std::string password;
std::string UID;


void showUsage(){
    std::string out = std::string("./user [-n ASIP] [-p ASport] [-m FSIP] [-q FSport]\n");
    write(1, out.c_str(), out.length());
    exit(EXIT_FAILURE);
}

bool isValidIP(char* ipAddr){
    std::string currentParam;
    int counter = 0;

    std::istringstream f(ipAddr);
    
    while(getline(f, currentParam, '.')){
        if(currentParam.find_first_not_of("[0123456789]") != std::string::npos){
            return false;
        }
        
        int number = std::stoi(currentParam);

        if(number > 255 || number < 0){
            return false;
        }

        counter++;
    }

    if(counter != 4){
        return false;
    }
    
    return true;
}

bool isInputValid(int argc, char** argv){
    if(argc > 10 ) 
    {
        return false;
    }
        //flags always come in pairs of flag -> value
    if((argc-1)%2 != 0){
        return false;
    }

    return true;
}

void processFlags(int argc, char** argv){

    for(int i = 1; i < argc; i+=2){
        if(!strcmp(argv[i], "-n")){
            if(!isValidIP(argv[i+1])){
                showUsage();
                exit(1);
            }
            ASIP = std::string(argv[i+1]);
        }
        else if(!strcmp(argv[i], "-m")){
            if(!isValidIP(argv[i+1])){
                showUsage();
                exit(1);
            }
            FSIP = std::string(argv[i+1]);

        }
        else if(!strcmp(argv[i], "-p")){
            ASport = std::string(argv[i+1]);
        }
        else if(!strcmp(argv[i], "-q")){
            FSPort = std::string(argv[i+1]);
        }
    }
}

int processInputArgs(int argc, char** argv) {
    if(!isInputValid(argc, argv)){
        showUsage();
        exit(1);
    }

    if(argc > 1){
        processFlags(argc, argv);
    }
    return 0;
}

void parseStdIn(TCPSocket* socket){
    std::string commandID;
    
    char inputString[128];

    if(fgets(inputString, 128, stdin) == nullptr) {
        perror("Error getting the input from the user.");
        exit(EXIT_FAILURE);
    }
    
    std::string command = std::string(inputString);

    auto components = redesHelperFunctions::getCommandComponents(command);
    commandID = components[0];
    if(commandID == "login" && components.size() == 3){
        UID = components[1];
        password = components[2];
        userAPI::login(UID, password, socket);
    }
    else if(commandID == "req") {
        std::string fileOperation;
        fileOperation = components[1];
        RID = redesHelperFunctions::getRandomTransactionCode();
        
        if ((fileOperation == "R" || fileOperation == "U" || fileOperation == "D") && components.size() == 3){
            std::string fileName = components[2];
            userAPI::requestOperation(UID, RID, fileOperation, fileName, socket);
        }
        else if ((fileOperation == "L" || fileOperation == "X" ) && components.size() == 2){
            userAPI::requestOperation(UID, RID, fileOperation, socket);
        }
        else {
            std::string outStr = "Error in req command: undefined file operation.\n";
            write(1, outStr.c_str(), outStr.length());
        }
    }
    else if(commandID == "val" && components.size() == 2){
        std::string validationCode;
        validationCode = components[1];
        std::string strRID = std::to_string(RID);
        userAPI::sendValidationCode(validationCode, UID, strRID, socket,TID);
    }
    else if((commandID == "list" || commandID == "l") && components.size() == 1){
        userAPI::list(UID, TID, FSIP, FSPort);
    }
    else if((commandID == "upload" || commandID == "u") && components.size() == 2){
        std::string filename = components[1];
        userAPI::uploadFile(UID, TID, FSIP, FSPort, filename);
    }
    else if((commandID == "retrieve" || commandID == "r") && components.size() == 2){
        std::string filename = components[1];
        userAPI::retrieveFile(UID, TID, FSIP, FSPort, filename);
    }
    else if((commandID == "delete" || commandID == "d") && components.size() == 2){
        std::string filename = components[1];
        userAPI::deleteFile(UID, TID, FSIP, FSPort, filename);
    }
    else if((commandID == "remove" || commandID == "x")  && components.size() == 1){
        userAPI::remove(UID, TID, FSIP, FSPort);
    }
    else if((commandID == "exit") && components.size() == 1){
        throw redesHelperFunctions::ExitSucessException();
    }
    else{
        std::string outStr = "Command not defined\n";
        write(1, outStr.c_str(), outStr.length());
    }
}

int main(int argc, char** argv){    
    //./user [-n ASIP] [-p ASport] [-m FSIP] [-q FSport]
    srand((unsigned int)time(NULL));
    if(int output = processInputArgs(argc, argv)){
        showUsage();
        exit(output);
    }

    auto socket = new TCPSocket(ASIP, ASport, AF_INET, 128);
    socket->tcpConnect();
    fd_set rfds;

    while(1){
        FD_ZERO(&rfds);
        FD_SET(0, &rfds);
        int selectReturn = select(1, &rfds, nullptr, nullptr, nullptr);
        
        if(selectReturn == -1){
            perror("select() Error!\n");
            exit(EXIT_FAILURE);
        }
        else if(selectReturn){
            try{
                if(FD_ISSET(0, &rfds)){
                    parseStdIn(socket);
                }
            }
            catch(const redesHelperFunctions::ExitSucessException&){
                //A successful execution path was reached, so we break out of the select loop so that an orderly exit 
                //the program can be done
                break;
            }
            
        }
    }
    delete socket;
    return 0;
}
