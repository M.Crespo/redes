#include "userAPI.hpp"


std::string userAPI::login (std::string UID, std::string& password, TCPSocket* socket){
    redesHelperFunctions::sanitizeString(password); 
    std::string payload = "LOG " + UID + " " + password + "\n";
    socket->sendPayload(payload);

    std::string response;
    socket->readPayload(response);
    if(response == "RLO OK\n"){
        std::cout << "Login successful." << std::endl;
    }
    else if(response == "RLO NOK\n"){
        std::cout << "Error logging in: Wrong password" << std::endl;
    }
    else if(response == "RLO ERR\n"){
        std::cout << "Error logging in: Wrong LOG Protocol." << std::endl;
    }
    else{
        std::cout << "Error: Wrong Protocol." << std::endl;
    }
    return response;
}

std::string userAPI::requestOperation (std::string UID, int RID, std::string& fileOperation, std::string& filename, TCPSocket* socket){
    // add padding to RID, eg 0004

    redesHelperFunctions::sanitizeString(filename);

    std::string payload = "REQ " + UID + " " + std::to_string(RID) + " " + fileOperation + " " + filename + "\n";
    
    socket->sendPayload(payload);

    std::string response;
    socket->readPayloadTimeout(response, 5);

    if (response == "RRQ OK\n"){
        std::cout << "Operation requesting successful." << std::endl;
    }
    else if (response == "RRQ ELOG\n"){
        std::cout << "Error requesting operation: please login first." << std::endl;
    }
    else if (response == "RRQ EPD\n"){
        std::cout << "Error requesting operation: AS could not reach PD." << std::endl;
    }
    else if (response == "RRQ EUSER\n"){
        std::cout << "Error requesting operation: incorrect UID." << std::endl;
    }
    else if (response == "RRQ EFOP\n"){
        std::cout << "Error requesting operation: invalid file operation." << std::endl;
    }
    else if(response == "RRQ ERR\n"){
        std::cout << "Error requesting operation: Wrong REQ Protocol." << std::endl;
    }
    else {
        std::cout << "Error: Wrong Protocol." << std::endl;
    }
    return response;
}

std::string userAPI::requestOperation (std::string UID, int RID, std::string& fileOperation, TCPSocket* socket){
    redesHelperFunctions::sanitizeString(fileOperation);
    std::string payload = "REQ " + UID + " " + std::to_string(RID) + " " + fileOperation + "\n";
    
    socket->sendPayload(payload);

    std::string response;
    socket->readPayload(response);
    if (response == "RRQ OK\n"){
        std::cout << "Operation requesting successful." << std::endl;
    }
    else if (response == "RRQ ELOG\n"){
        std::cout << "Error requesting operation: please login first." << std::endl;
    }
    else if (response == "RRQ EPD\n"){
        std::cout << "Error requesting operation: AS could not reach PD." << std::endl;
    }
    else if (response == "RRQ EUSER\n"){
        std::cout << "Error requesting operation: incorrect UID." << std::endl;
    }
    else if (response == "RRQ EFOP\n"){
        std::cout << "Error requesting operation: invalid file operation." << std::endl;
    }
    else if(response == "RRQ ERR\n"){
        std::cout << "Error requesting operation: Wrong REQ Protocol." << std::endl;
    }
    else {
        std::cout << "Error: Wrong Protocol." << std::endl;
    }
    return response;
}

std::string userAPI::sendValidationCode(std::string& code, std::string& userIdentifier, std::string& requestID, TCPSocket* socket, std::string& TID){
    std::string payload = "AUT " + userIdentifier + " " + requestID + " " + code + "\n";

    socket->sendPayload(payload);
    std::string response;

    socket->readPayload(response);

    std::vector<std::string> components = redesHelperFunctions::getCommandComponents(response);
    TID = components[1];

    if (TID != "0"){
        std::cout << "Validation successful." << std::endl;   
    } else {
        std::cout << "Error validating operation: wrong code." << std::endl;
    }
    return response;
}

std::string userAPI::list(std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort){
    
    auto socket = new TCPSocket(fsIp, fsPort, AF_INET, 128);
    socket->tcpConnect();
    
    std::string payload = "LST " + UID + " " + TID + "\n";
    
    socket->sendPayload(payload);

    std::string response;
    socket->readPayload(response);

    if (response == "RLS EOF\n"){
        std::cout << "Error listing: no files available." << std::endl;
    }
    else if (response == "RLS INV\n"){
        std::cout << "Error listing: AS validation error." << std::endl;
    }
    else if (response == "RLS ERR\n"){
        std::cout << "Error listing: Unexpected Protocol." << std::endl;
    }
    else if (response == "ERR\n"){
        std::cout << "Error: Wrong Protocol." << std::endl;
    }
    else {
        auto responseComponents = redesHelperFunctions::getCommandComponents(response);
        std::cout << "Files:" << std::endl;
        for (uint i=2; i < responseComponents.size(); i+=2){
            std::cout << std::to_string(i/2) + ": " + responseComponents[i] + ", " + responseComponents[i+1] + "." << std::endl;
        }
    }

    delete socket;
    return response;
}

std::string userAPI::uploadFile (std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort, std::string& filename){
    auto socket = new TCPSocket(fsIp, fsPort, AF_INET, 128);
    socket->tcpConnect();
    
    int size = redesHelperFunctions::getFileSize("upload_files/" + filename);

    // TODO: Not really a TODO but maybe ask why this only works if it has a \n

    std::string payload = "UPL " + UID + " " + TID + " " + filename + " " + std::to_string(size) + " ";
    socket->sendPayload(payload);
    redesHelperFunctions::readFromFileToSocket(socket->getFD(), size, "upload_files/" + filename);

    std::string response;
    socket->readPayload(response);

    if (response == "RUP OK\n"){
        std::cout << filename + " was successfully uploaded." << std::endl;
    }
    else if (response == "RUP NOK\n"){
        std::cout << "Error uploading "+ filename +": invalid UID." << std::endl;
    }
    else if (response == "RUP DUP\n"){
        std::cout << "Error uploading "+ filename +": file already exists" << std::endl;
    }
    else if (response == "RUP FULL\n"){
        std::cout << "Error uploading "+ filename +": server is full (more than 15 files uploaded)" << std::endl;
    }
    else if (response == "RUP INV\n"){
        std::cout << "Error uploading: AS validation error." << std::endl;
    }
    else if (response == "RUP ERR\n"){
        std::cout << "Error uploading: Unexpected Protocol." << std::endl;
    }    
    else {
        std::cout << "Error: Wrong Protocol." << std::endl;
    }
    delete socket;
    return response;
}

std::string userAPI::retrieveFile(std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort, std::string& filename){
    auto socket = new TCPSocket(fsIp, fsPort, AF_INET, 128);
    socket->tcpConnect();
    
    std::string payload = "RTV " + UID + " " + TID + " " + filename + "\n";
    
    socket->sendPayload(payload);

    std::string response, data;

    socket->readPayloadTimeout(response, 5);

    if (response.find("RRT OK") != std::string::npos){
        auto components = redesHelperFunctions::getCommandComponents(response);
        std::string fSize = components[2];
        std::string data;
        
        redesHelperFunctions::readFromSocketToFile(socket->getFD(), stoi(fSize), "user_dir/files/" + filename);
    }
    else if (response == "RRT NOK\n"){
        std::cout << "File retrieval failed: File does not exist." << std::endl;
    }
    else if (response == "RRT INV\n"){
        std::cout << "File retrieval failed: AS validation error." << std::endl;
    }
    else if (response == "RRT ERR\n"){
        std::cout << "File retrieval failed: Unexpected Protocol." << std::endl;
    }
    else {
        std::cout << "Error: Wrong Protocol." << std::endl;
    }
    delete socket;
    return response;
}

std::string userAPI::deleteFile (std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort, std::string& filename){
    auto socket = new TCPSocket(fsIp, fsPort, AF_INET, 128);
    socket->tcpConnect();

    std::string payload = "DEL " + UID + " " + TID + " " + filename + "\n";
    
    socket->sendPayload(payload);

    std::string response;
    socket->readPayload(response);

    if (response == "RDL OK\n"){
        std::cout << "File deletion successful." << std::endl;
    }
    else if (response == "RDL EOF\n"){
        std::cout << "Error deleting file: file is not available." << std::endl;
    }
    else if (response == "RDL NOK\n"){
        std::cout << "Error deleting file: UID does not exist." << std::endl;
    }
    else if (response == "RDL INV\n"){
        std::cout << "Error deleting file: AS validation error." << std::endl;
    }
    else if(response == "RDL ERR\n"){
        std::cout << "Error deleting file: Unexpected Protocol." << std::endl;
    }
    else {
        std::cout << "Error: Wrong Protocol." << std::endl;
    }
    delete socket;
    return response;
}

std::string userAPI::remove (std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort){
    auto socket = new TCPSocket(fsIp, fsPort, AF_INET, 128);
    socket->tcpConnect();

    std::string payload = "REM " + UID + " " + TID + "\n";

    socket->sendPayload(payload);

    std::string response;
    socket->readPayload(response);

    if (response == "RRM OK\n"){
        std::cout << "User removal successful." << std::endl;
    }
    else if (response == "RRM NOK\n"){
        std::cout << "Error removing user: UID does not exist." << std::endl;
    }
    else if (response == "RRM INV\n"){
        std::cout << "Error removing user: AS validation error." << std::endl;
    }
    else if(response == "RRM ERR\n"){
        std::cout << "Error removing user: Unexpected Protocol." << std::endl;
    }
    else {
        std::cout << "Error: Wrong Protocol." << std::endl;
    }
    
    delete socket;
    return response;
}



