#ifndef USERAPI_HPP
#define USERAPI_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

#include "../shared/TCPSocket.hpp"
#include "../shared/helperFunctions.hpp"


//All methods user can use to interact with the servers
namespace userAPI{
    std::string login (std::string UID, std::string& password, TCPSocket* socket);
    std::string requestOperation (std::string UID, int RID, std::string& fileOperation, TCPSocket* socket);
    std::string requestOperation (std::string UID, int RID, std::string& fileOperation, std::string& filename, TCPSocket* socket);
    std::string sendValidationCode(std::string& code, std::string& userIdentifier, std::string& requestID, TCPSocket* socket, std::string& TID);
    std::string list (std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort);
    std::string retrieveFile (std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort, std::string& filename);
    std::string uploadFile (std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort, std::string& filename);
    std::string deleteFile (std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort, std::string& filename);
    std::string remove (std::string& UID, std::string& TID, std::string& fsIp, std::string& fsPort);
    std::string exitUser ();
}
#endif