#ifndef TCPSOCKET_HPP
#define TCPSOCKET_HPP

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <fcntl.h>
#include <string>
#include <iostream>

#include "AbstractSocket.hpp"

class TCPSocket : public AbstractSocket {
    public:
        void sendPayload(std::string payload);
        void sendPayload(int fd, std::string payload);
        void sendPayload(int fd, int size, std::string payload);
        void sendPayload(std::string payload, int size);

        int readPayloadTimeout(std::string& payload, int timeoutDiff);
        int readPayload(std::string& str);
        void readPayload(std::string& str, int size);
        int readPayload(int fd, std::string& str);
        int readPayload(int fd, int size, std::string& str);
        TCPSocket(std::string ip, std::string port, int aiFamily, int bufferSize);
        TCPSocket(std::string port, int bufferSize);
        virtual ~TCPSocket();
        int getFD();
        void setAIFlags(int aiFlag);
        struct addrinfo* getRes();
        void tcpConnect();
    private:
        int fd;
        int bufferSize;
        struct sockaddr_in  addr;
        struct addrinfo hints, *destAddrInfo;
        socklen_t addrlen;
};
#endif