#include "UDPSocket.hpp"
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>


UDPSocket::UDPSocket(std::string ip, std::string port, int aiFamily, int bufferSize){
    this->fd = socket(aiFamily, SOCK_DGRAM, 0);

    if(this->fd == -1){ 
        perror("UDPSOCKET: Error creating.\n");
        exit(EXIT_FAILURE);
    }
    this->bufferSize = bufferSize;

    memset(&(this->hints),0,sizeof(sockaddr_in));

    this->hints.ai_family = aiFamily;
    this->hints.ai_socktype = SOCK_DGRAM;

    int errcode = getaddrinfo(ip.c_str(), port.c_str(), &(this->hints), &(this->destAddrInfo));
 

    if(errcode != 0 ){
        perror("UDPSOCKET: Error getting info from socket endpoint.");
        exit(EXIT_FAILURE);
    }
}

UDPSocket::UDPSocket(const std::string& port, int bufferSize){
    int errcode;
    struct addrinfo hints;
    this->bufferSize = bufferSize;
    this->fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(this->fd == -1){ 
        perror("UDPSOCKET: Error creating.\n");
        exit(EXIT_FAILURE);
    }
    bzero(&hints, sizeof(hints));
    this->hints.ai_family = AF_INET;
    this->hints.ai_socktype = SOCK_DGRAM;
    this->hints.ai_flags = AI_PASSIVE;
    errcode = getaddrinfo(NULL, port.c_str(), &(this->hints), &(this->destAddrInfo));
    if(errcode != 0 ){
        perror("UDPSOCKET: Failed getaddrinfo.");
        exit(EXIT_FAILURE);
    }
}


UDPSocket::~UDPSocket(){
    freeaddrinfo(this->destAddrInfo);
    this->closeConnections();
}

void UDPSocket::closeConnections(){
    //check if file decriptor is valid, in case someone already closed the connection before
    //if(fcntl(this->fd, F_GETFD)){
        if(close(this->fd) != 0){
            perror("UDPSOCKET: Error closing socket file descriptor.\n");
            exit(errno);
        }
    //}
}

void UDPSocket::sendPayload(std::string payload){
    int n = sendto(fd, payload.c_str(), payload.length(),
                       0, this->destAddrInfo->ai_addr, this->destAddrInfo->ai_addrlen);

    if(n == -1){
        perror("UDPSOCKET: Error sending payload.\n");
        exit(EXIT_FAILURE);
    }
}

void UDPSocket::sendPayload(std::string payload, const std::string& ip, const std::string& port){
    struct addrinfo * addrInfo;
    int errcode = getaddrinfo(ip.c_str(), port.c_str(), &(this->hints), &(addrInfo));

    if(errcode){
        perror("UDPServer: error getting addrinfo.");
        exit(EXIT_FAILURE);
    }

    int n = sendto(fd, payload.c_str(), payload.length(),
                       0, addrInfo->ai_addr, addrInfo->ai_addrlen);

    if(n == -1){
        perror("UDPSOCKET: Error sending payload.\n");
        exit(EXIT_FAILURE);
    }
}

int UDPSocket::readPayload(std::string& str){
    char buffer[this->bufferSize];
    this->addrlen = sizeof((this->addr));
    
    struct timeval tv = {this->getReadTimeout(),0};
    setsockopt(this->fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
    int n = recvfrom(this->fd, buffer, this->bufferSize,0, (struct sockaddr*) &(this->addr), &addrlen);

    if(n == -1){
        if(errno == EAGAIN){
            return -1;
        }
        perror("UDPSOCKET: Error reading payload.\n");
        exit(EXIT_FAILURE);
    }
    this->destAddrInfo->ai_addr = (struct sockaddr*) &(this->addr);
    this->destAddrInfo->ai_addrlen = this->addrlen;
    str.assign(buffer, n);
    return n;
}

int UDPSocket::getFD(){
    return this->fd;
}

void UDPSocket::setAIFlags(int aiFlag){
    this->destAddrInfo->ai_flags = aiFlag;
}

struct addrinfo* UDPSocket::getRes(){
    return this->destAddrInfo;
}

std::pair<std::string, std::string> UDPSocket::getAddressInfo(){

    std::string ip = std::string(inet_ntoa(this->addr.sin_addr));
    std::string port = std::to_string(ntohs(this->addr.sin_port));
    return std::pair<std::string, std::string>(ip, port);
}