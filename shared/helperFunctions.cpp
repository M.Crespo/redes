#include "helperFunctions.hpp"

std::vector<std::string> redesHelperFunctions::getCommandComponents(std::string str){
    std::vector<std::string> stringComponents;
    str.push_back(' '); //make sure all components are processed
    std::string currentParameter;
    std::string delim = " ";
    size_t position;
    if((position = str.find(delim)) == std::string::npos){
        sanitizeString(str);
        stringComponents.push_back(str);
    }
    else
    {
        while((position = str.find(delim)) != std::string::npos){
            currentParameter = str.substr(0, position);
            sanitizeString(currentParameter);
            stringComponents.push_back(currentParameter);
            str.erase(0, position + delim.length());
        }
    }

    return stringComponents;
}

void redesHelperFunctions::sanitizeString(std::string& str){
    str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
}

int redesHelperFunctions::getRandomTransactionCode(){
    return generateRandomNumber(1000, 10000);
}

bool redesHelperFunctions::checkNumericOnlyString(const std::string& str){
    return (str.find_first_not_of("[0123456789]") == std::string::npos);
}

bool redesHelperFunctions::isValidIP(const std::string& ipAddr){
    std::string currentParam;
    int counter = 0;

    std::istringstream f(ipAddr);
    
    while(getline(f, currentParam, '.')){
        if(currentParam.find_first_not_of("[0123456789]") != std::string::npos){
            return false;
        }
        
        int number = std::stoi(currentParam);

        if(number > 255 || number < 0){
            return false;
        }

        counter++;
    }

    if(counter != 4){
        return false;
    }
    
    return true;
}
void redesHelperFunctions::writeToFile(const std::string& filePath, const std::string& content){
    std::ofstream tidFile(filePath);

    tidFile << content;

    tidFile.close();

    return;
}

bool redesHelperFunctions::fileExists(const std::string& pathToFile){
    return (access(pathToFile.data(), F_OK) != -1);
}

 bool redesHelperFunctions::checkIfDirectoryExists(const std::string& pathToDir){
    struct stat buffer;
    return (stat(pathToDir.c_str(), &buffer) == 0);
}

void redesHelperFunctions::readFromFile(const std::string& pathToFile, std::vector<std::string>& lines){
    if(fileExists(pathToFile)){
        std::ifstream openedFile(pathToFile);
        std::string currentLine;

        while(getline(openedFile, currentLine)){
            lines.push_back(currentLine);
        }
    }
}
std::string redesHelperFunctions::readFromFileToString(const std::string& pathToFile){
    if(fileExists(pathToFile)){
        std::ifstream openedFile(pathToFile);
        std::string currentLine;
        std::string content;

        while(getline(openedFile, currentLine)){
            content.append(currentLine + "\n");
        }
        return content;
    }
    return "";
}

void redesHelperFunctions::readFromSocketToFile(int fd, int size, const std::string& filename){
    FILE *outputFile = fopen(filename.data(), "w");
    int nread;
    int readSize = 2048;
    char buffer[readSize];
    int nLeft = size;
    while (nLeft > 0){
        nread = read(fd, buffer, readSize);

        if(nread == -1){
            perror("readFromSocketToFile: Error reading from socket");
            exit(1);
        } else if (nread == 0){
            break;
        }
        nLeft -= nread;
        //Avoid writing the final \n to the file
        if(nLeft <= 0){
            fwrite(buffer,1,nread-1,outputFile);
        }
        else{
            fwrite(buffer,1,nread,outputFile);
        }
        
    }
    fclose(outputFile);  
}

void redesHelperFunctions::readFromFileToSocket(int fd, int size, const std::string& filename){
    FILE *inputFile = fopen(filename.data(), "r");
    int nwritten = 0, nwrote = 0;
    int readSize = 2048;
    char buffer[readSize];
    while (nwritten < size){
        
        fread(buffer,readSize, 1, inputFile);
        nwrote = write(fd, buffer, readSize);
        if(nwrote == -1){
            perror("readFromFileToSocket: Error sending from socket");
            exit(1);
        } else if (nwrote == 0){
            break;
        }
        nwritten += nwrote;
    }
    if(write(fd, " \n", 2) < 1){
        perror("readFromFileToSocket (newline): Error sending from socket");
        exit(1);
    }
    fclose(inputFile);  
}

void redesHelperFunctions::deleteFile(const std::string& pathToFile){
    int n = 0;
    if(fileExists(pathToFile))
        n = remove(pathToFile.data());

    if(n == -1){
        perror("Error deleting file");
        exit(EXIT_FAILURE);
    }
}

void redesHelperFunctions::deleteDirectory(const std::string& pathToDir){
    std::vector<std::string> files;
    redesHelperFunctions::ListDir(pathToDir, files);
    
    for (std::string file : files){
        redesHelperFunctions::deleteFile(pathToDir + file);
    }

    rmdir(pathToDir.data());
}

int redesHelperFunctions::ListDir(const std::string& dirname, std::vector<std::string>& fileNames){
    DIR *d;
    struct dirent *dir;
    d=opendir(dirname.c_str());
    if(d){
        while((dir=readdir(d)) !=NULL){
            if(std::string(dir->d_name) != "." && std::string(dir->d_name) != ".."){
                fileNames.push_back(std::string(dir->d_name));
            }
        }
        closedir(d);
        return(1);
    }
    else
    return(0);
}

int redesHelperFunctions::getFileSize(const std::string& filePath){
    struct stat stat_buf;
    int rc = stat(filePath.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}


int redesHelperFunctions::generateRandomNumber(int min, int max){
    std::srand(time(0));
    int validationCode = std::rand() % max;
    if(validationCode < min){validationCode += min;};
    return validationCode;
}

