#ifndef UDPSERVER_HPP
#define UDPSERVER_HPP

#include <iostream>
#include <string>
#include "UDPSocket.hpp"
#include <sys/socket.h>


class UDPServer{

    public:
        UDPServer(std::string ip,std::string port, int aiFamily, int bufferSize);
        UDPServer(std::string port, int bufferSize);

        int readPayload(std::string& readString);
        void sendPayload(std::string stringToSend, const std::string& ip, const std::string& port);
        void sendPayload(std::string stringToSend);

        int getFileDescriptor();

        std::pair<std::string, std::string> getAddressInfo();
        
        ~UDPServer();

    private:

        void bindSocket();
        bool hasRead = false;
        UDPSocket* sock;
};

#endif