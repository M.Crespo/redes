#include "TCPServer.hpp"


TCPServer::TCPServer(std::string ip,std::string port, int aiFamily, int bufferSize){
    this->socket = new TCPSocket(ip, port, aiFamily, bufferSize);

    this->bindSocket();
    this->tcpListen(10);
}

TCPServer::TCPServer(std::string port, int bufferSize){
    this->socket = new TCPSocket(port, bufferSize);

    this->bindSocket();
    this->tcpListen(10);
}


void TCPServer::bindSocket(){
    struct addrinfo* sockRes = this->socket->getRes();
    int n = bind(this->socket->getFD(), sockRes->ai_addr, sockRes->ai_addrlen);

    if(n == -1){
        perror("TCPSERVER: Error binding socket");
        exit(EXIT_FAILURE);
    }
}

int TCPServer::readPayload(int fd, std::string& readString){
    int n = this->socket->readPayload(fd, readString);
    this->hasRead = true;
    return n;
}

int TCPServer::readPayload(int fd, int size, std::string& readString){
    int n = this->socket->readPayload(fd, size, readString);
    return n;
}


void TCPServer::sendPayload(std::string stringToSend){
    //This function sends the payload to the last device that sent us a message.
    //This device may changed based on previous calls to TCPServer::readPayload
    if (this->hasRead)
        this->socket->sendPayload(stringToSend);
}
void TCPServer::sendPayload(int fd, std::string stringToSend){
    //This function sends the payload to the last device that sent us a message.
    //This device may changed based on previous calls to TCPServer::readPayload
    this->socket->sendPayload(fd, stringToSend);
}

void TCPServer::sendPayload(int fd, int size, std::string stringToSend){
    //This function sends the payload to the last device that sent us a message.
    //This device may changed based on previous calls to TCPServer::readPayload
    this->socket->sendPayload(fd, size, stringToSend);
}


TCPServer::~TCPServer(){
    delete this->socket;
}

int TCPServer::getFileDescriptor(){
    return this->socket->getFD();
}

void TCPServer::tcpListen(const int maxConnections){
    if(listen(this->getFileDescriptor(), maxConnections) == - 1){
        perror("TCPServer: error on listen");
        exit(EXIT_FAILURE);
    }
}

int TCPServer::tcpAccept(std::string& ip, std::string& port){
    int fd = -1;
    struct sockaddr_in addr;
    socklen_t addrLength = sizeof(addr);
    if((fd = accept(this->getFileDescriptor(), (struct sockaddr*) &addr, &addrLength)) < 0){
        perror("TCPServer: error on accept");
        exit(EXIT_FAILURE);
    }
    
    ip = std::string(inet_ntoa(addr.sin_addr));
    port = std::to_string(ntohs(addr.sin_port));
    return fd;
}