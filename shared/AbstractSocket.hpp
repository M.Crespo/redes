#ifndef ABSTRACTSOCKET_HPP
#define ABSTRACTSOCKET_HPP

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <time.h>

#include <string>
#include <iostream>

class AbstractSocket {
    public:
        virtual void sendPayload(std::string payload) = 0;
        virtual int readPayload(std::string& str) = 0;
        AbstractSocket(){};
        virtual ~AbstractSocket(){};

        int getReadTimeout(){return this->readTimeout;};

    private:
        int readTimeout = 5;
};

#endif