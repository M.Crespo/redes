#ifndef TCPSERVER_HPP
#define TCPSERVER_HPP

#include <iostream>
#include <netinet/in.h>
#include <string>
#include "TCPSocket.hpp"


class TCPServer{

    public:
        TCPServer(std::string ip,std::string port, int aiFamily, int bufferSize);
        TCPServer(std::string port, int bufferSize);

        int readPayload(int fd, std::string& readString);
        int readPayload(int fd, int size, std::string& readString);

        void sendPayload(std::string stringToSend);
        void sendPayload(int fd, std::string stringToSend);
        void sendPayload(int fd, int size, std::string stringToSend);

        int getFileDescriptor();

        int tcpAccept(std::string& ip, std::string& port);

        ~TCPServer();

    private:
        void tcpListen(const int maxConnections);
        void bindSocket();
        bool hasRead = false;
        TCPSocket* socket;
};

#endif