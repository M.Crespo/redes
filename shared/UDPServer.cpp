#include "UDPServer.hpp"



UDPServer::UDPServer(std::string ip,std::string port, int aiFamily, int bufferSize){
    this->sock = new UDPSocket(ip, port, aiFamily, bufferSize);

    this->sock->setAIFlags(AI_PASSIVE);

    this->bindSocket();
}

UDPServer::UDPServer(std::string port, int bufferSize){

    this->sock = new UDPSocket(port, bufferSize);

    this->sock->setAIFlags(AI_PASSIVE);

    this->bindSocket();
}

void UDPServer::bindSocket(){
    struct addrinfo* sockRes = this->sock->getRes();
    int n = bind(this->sock->getFD(), sockRes->ai_addr, sockRes->ai_addrlen);

    if(n == -1){
        perror("UDPSERVER: Error binding socket\n");
        exit(EXIT_FAILURE);
    }
}

int UDPServer::readPayload(std::string& readString){
    int n = this->sock->readPayload(readString);
    this->hasRead = true;
    return n;
}

void UDPServer::sendPayload(std::string stringToSend){
    //This function sends the payload to the last device that sent us a message.
    //This device may changed based on previous calls to UDPServer::readPayload
    if (this->hasRead)
        this->sock->sendPayload(stringToSend);
}

UDPServer::~UDPServer(){
    delete this->sock;
}

int UDPServer::getFileDescriptor(){
    return this->sock->getFD();
}

std::pair<std::string, std::string> UDPServer::getAddressInfo(){
    return this->sock->getAddressInfo();
}