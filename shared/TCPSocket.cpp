#include "TCPSocket.hpp"

TCPSocket::TCPSocket(std::string ip, std::string port, int aiFamily, int bufferSize){
    this->fd = socket(AF_INET, SOCK_STREAM, 0);
    if(this->fd == -1) exit(1);
    this->bufferSize = bufferSize;

    memset(&(this->hints),0,sizeof(sockaddr_in));

    this->hints.ai_family = aiFamily;
    this->hints.ai_socktype = SOCK_STREAM;


    int errcode = getaddrinfo(ip.c_str(), port.c_str(), &(this->hints), &(this->destAddrInfo));

    if(errcode != 0 ) exit(errcode);
}

TCPSocket::TCPSocket(std::string port, int bufferSize){
    int errcode;
    this->bufferSize = bufferSize;
    this->fd = socket(AF_INET, SOCK_STREAM, 0);
    if(this->fd == -1){ 
        perror("TCPSocket: Error creating.\n");
        exit(EXIT_FAILURE);
    }
    this->hints.ai_family = AF_INET;
    this->hints.ai_socktype = SOCK_STREAM;
    this->hints.ai_flags = AI_PASSIVE;
    errcode = getaddrinfo(nullptr, port.c_str(), &(this->hints), &(this->destAddrInfo));
    if(errcode != 0 ){
        perror("TCPSocket: Failed getaddrinfo.");
        exit(EXIT_FAILURE);
    }
}

TCPSocket::~TCPSocket(){
    freeaddrinfo(this->destAddrInfo);
    if(fcntl(this->fd, F_GETFD)){
        if(close(this->fd) != 0){
            perror("TCPSocket: Error closing socket file descriptor.\n");
            exit(errno);
        }
    }
}

void TCPSocket::sendPayload(std::string payload){
    this->sendPayload(this->fd, payload.size(), payload);
}

void TCPSocket::sendPayload(int fd, std::string payload){
    this->sendPayload(fd, payload.size(), payload);
}

void TCPSocket::sendPayload(int fd, int size, std::string payload){
    uint nLeft = size;
    int n = 0;
    while(nLeft > 0){
        payload = payload.substr(n, payload.size());
        //struct timeval tv = {this->getReadTimeout(),0};
        //setsockopt(this->fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
        n = write(fd, payload.c_str(), nLeft);

        if (n < 1)
            break;
        nLeft -= n;
    }
    if(n == -1){
        perror("TCPSocket: Error sending to socket");
        exit(1);
    }
}

void TCPSocket::sendPayload(std::string payload, int size){
    uint nLeft = size;
    int n = 0;
    while(nLeft > 0){
        payload = payload.substr(n, payload.size());
        //struct timeval tv = {this->getReadTimeout(),0};
        //setsockopt(this->fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
        n = write(this->fd, payload.c_str(), nLeft);

        if (n < 1)
            break;
        nLeft -= n;
    }
    if(n == -1){
        perror("TCPSocket: Error sending to socket");
        exit(1);
    }
}

int TCPSocket::readPayloadTimeout(std::string& str, int timeoutDiff){
    char buffer[this->bufferSize];
    struct timeval tv = {this->getReadTimeout()+timeoutDiff,0};
    setsockopt(this->fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
    int n = read(this->fd, buffer, this->bufferSize);
    if(n == -1){
        if(errno == EAGAIN){
            return -1;
        }
        perror("TCPSocket: Error reading(time) from socket");
        exit(1);
    }
    str.assign(buffer, n);
    return n;

}

int TCPSocket::readPayload(std::string& str){
    return TCPSocket::readPayloadTimeout(str, 0);
}

int TCPSocket::readPayload(int fd, std::string& str){
    char buffer[this->bufferSize];
    
    struct timeval tv = {this->getReadTimeout(),0};
    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
    int n = read(fd, buffer, this->bufferSize);
    if(n == -1){
        if(104 == errno){
            return 0;
        }
        perror("TCPSocket(2): Error reading from socket");
        exit(1);
    }
    str.assign(buffer, n);
    return n;
}

int TCPSocket::readPayload(int fd, int size, std::string& str){
    uint readSize = 128;
    char buffer[readSize];
    uint nLeft = size;
    int n;
    do{
        // struct timeval tv = {this->getReadTimeout(),0};
        // setsockopt(this->fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
        n = read(fd, buffer, readSize);

        str.append(std::string(buffer).substr(0,n));
        nLeft -= n;
        if(n == -1){
            perror("TCPSocket: Error reading(3) from socket");
            exit(1);
        }
    }
    while(n != 0);


    return n;
}

void TCPSocket::readPayload(std::string& str, int size){
    char buffer[size];
    uint nLeft = size;
    int n = 0;
    while(nLeft > 0){
        struct timeval tv = {this->getReadTimeout(),0};
        setsockopt(this->fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));
        n = read(this->fd, buffer, nLeft);
        if (n < 1)
            break;
        str.append(std::string(buffer).substr(0,n));
        nLeft -= n;
    }
    if(n == -1){
        perror("TCPSocket: Error reading from socket");
        exit(1);
    }
}


int TCPSocket::getFD(){
    return this->fd;
}

void TCPSocket::setAIFlags(int aiFlag){
    this->destAddrInfo->ai_flags = aiFlag;
}

struct addrinfo* TCPSocket::getRes(){
    return this->destAddrInfo;
}

void TCPSocket::tcpConnect(){
    int connectionCode = connect(this->fd, this->destAddrInfo->ai_addr, this->destAddrInfo->ai_addrlen);

    if(connectionCode != 0 ){
        perror("TCPSocket: Error Connecting to server");
        exit(connectionCode);
    }
}
