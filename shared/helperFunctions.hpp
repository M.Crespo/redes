#ifndef HELPER_FUNCTIONS_HPP
#define HELPER_FUNCTIONS_HPP
#include <string>
#include <iostream>
#include <string.h>
#include <vector>
#include <sstream>
#include <algorithm>
#include <cstdlib>
#include <dirent.h>
#include <fstream>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

namespace redesHelperFunctions{
    
    std::vector<std::string> getCommandComponents(std::string str);

    void sanitizeString(std::string& str);

    int getRandomTransactionCode();

    bool checkNumericOnlyString(const std::string& str);

    bool isValidIP(const std::string& ipAddr);

    void writeToFile(const std::string& filePath, const std::string& content);

    void readFromSocketToFile(int fd, int size,const std::string& filename);
    
    void readFromFileToSocket(int fd, int size,const std::string& filename);

    void readFromFile(const std::string& filePath, std::vector<std::string>& lines);

    int getFileSize(const std::string& filePath);

    int ListDir(const std::string& dirname, std::vector<std::string>& fileNames);

    bool fileExists(const std::string& pathToFile);

    void deleteFile(const std::string& pathToFile);

    void deleteDirectory(const std::string& pathToDir);

    int generateRandomNumber(int min, int max);

    bool checkIfDirectoryExists(const std::string& pathToDir);

    std::string readFromFileToString(const std::string& pathToFile);


    typedef struct request{
        std::string UID;
        std::string validationCode;
        std::string requestID;
        std::string FOP;
        std::string filename;
    }request;

    typedef struct fdInfo {
        bool isActive = true;
        std::string ip;
        std::string port;
        std::string UID;
    } fdInfo;
    struct ExitSucessException : public std::exception {
        const char* what() const throw() {
            return "Exit Success Exception\n";
        }
    };
}

#endif