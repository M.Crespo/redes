#ifndef UDPSOCKET_HPP
#define UDPSOCKET_HPP

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <string>
#include <iostream>

#include "AbstractSocket.hpp"

class UDPSocket : public AbstractSocket {
    public:
        void sendPayload(std::string payload);
        int readPayload(std::string& str);
        UDPSocket(std::string ip, std::string port, int aiFamily, int bufferSize);
        UDPSocket(const std::string& port, int bufferSize);

        virtual ~UDPSocket();
        void closeConnections();
        int getFD();
        void setAIFlags(int flag);
        struct addrinfo* getRes();

        void sendPayload(std::string payload, const std::string& ip, const std::string& port);
        std::pair<std::string, std::string> getAddressInfo();

    private:
        int fd;
        int bufferSize;
        struct sockaddr_in addr;
        struct addrinfo hints, *destAddrInfo;
        socklen_t addrlen;
};
#endif