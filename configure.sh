#!/bin/bash
ASIP=$1
FSIP=$2

if [ $# -eq 2 ]; then
    echo "Changing program IPs..."
    echo "Changing ASIP in as.cpp"
    sed -i 's/ASIP =.*/ASIP = "'$ASIP'";/g' ./as_dir/as.cpp
    echo "Changing ASIP in fs.cpp"
    sed -i 's/ASIP =.*/ASIP = "'$ASIP'";/g' ./fs_dir/fs.cpp
    echo "Changing FSIP in fs.cpp"
    sed -i 's/FSIP =.*/FSIP = "'$FSIP'";/g' ./fs_dir/fs.cpp
else
    echo "Usage: ./configure.sh AS_IP FS_IP"
fi