#ifndef PROCESSCOMMANDS_HPP
#define PROCESSCOMMANDS_HPP

#include <iostream>
#include <map>
#include <time.h>

#include "../shared/helperFunctions.hpp"
#include "../shared/UDPServer.hpp"
#include "../shared/TCPServer.hpp"

#include "Client.hpp"

void processREGCommand(UDPServer *udpServer, const std::vector<std::string> &components, bool verbose);

void processUNRCommand(UDPServer *udpServer, const std::vector<std::string> &components, bool verbose);

void processRVCCommand(UDPServer *udpServer, const std::vector<std::string> &components, bool verbose);

bool processLOGCommand(TCPServer* tcpServer, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose);

redesHelperFunctions::request processREQCommand(TCPServer* tcpServer, UDPServer* udpServer, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose);
void processAUTCommand(TCPServer* tcpServer, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose, std::vector<redesHelperFunctions::request>& requests);


void processVLDCommand(UDPServer* server, const std::vector<std::string> &components, bool verbose);


#endif