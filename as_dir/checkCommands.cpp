#include "checkCommands.hpp"

bool checkREGCommand(const std::string &command)
{
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);

    return (*((command.end()) - 1) == '\n') &&
           (commandComponents.size() == 5) &&
           (commandComponents[0] == "REG") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 8) &&
           (redesHelperFunctions::isValidIP(commandComponents[3])) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[4]));
}

bool checkUNRCommand(const std::string &command)
{
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);

    return (*((command.end()) - 1) == '\n') &&
           (commandComponents.size() == 3) &&
           (commandComponents[0] == "UNR") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 8);
}

bool checkVLCCommand(const std::string &command)
{
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);

    if ((commandComponents[3] == "X" || commandComponents[3] == "L") && commandComponents.size() != 4)
    {
        return false;
    }
    else if ((commandComponents[3] == "R" || commandComponents[3] == "U" || commandComponents[3] == "D") && commandComponents.size() != 5)
    {
        return false;
    }

    return (*((command.end()) - 1) == '\n') &&
           (commandComponents[0] == "VLC") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 4);
}

bool checkRVCCommand(const std::string &command)
{
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);

    return (*((command.end()) - 1) == '\n') &&
           (commandComponents[0] == "RVC") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2] == "OK" || commandComponents[2] == "NOK");
}

bool checkLOGCommand(const std::string &command)
{
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);

    return (*((command.end()) - 1) == '\n') &&
           (commandComponents.size() == 3) &&
           (commandComponents[0] == "LOG") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 8);
}

bool checkREQCommand(const std::string &command){
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);

    if ((commandComponents[3] == "X" || commandComponents[3] == "L") && commandComponents.size() != 4)
    {
        return false;
    }
    else if ((commandComponents[3] == "R" || commandComponents[3] == "U" || commandComponents[3] == "D") && commandComponents.size() != 5)
    {
        return false;
    }

    return (*((command.end()) - 1) == '\n') &&
            (commandComponents[0] == "REQ") &&
            (commandComponents[1].size() == 5) &&
            (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
            (commandComponents[2].size() == 4) && 
            (redesHelperFunctions::checkNumericOnlyString(commandComponents[2]));
}

bool checkAUTCommand(const std::string &command){
        auto commandComponents = redesHelperFunctions::getCommandComponents(command);

        return (*((command.end()) - 1) == '\n') &&
            (commandComponents[0] == "AUT") &&
            (commandComponents[1].size() == 5) &&
            (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
            (commandComponents[2].size() == 4) && 
            (redesHelperFunctions::checkNumericOnlyString(commandComponents[2])) &&
            (commandComponents[3].size() == 4) && 
            (redesHelperFunctions::checkNumericOnlyString(commandComponents[3]));
}

bool checkVLDCommand(const std::string &command){
    auto commandComponents = redesHelperFunctions::getCommandComponents(command);

    return (*((command.end()) - 1) == '\n') &&
           (commandComponents.size() == 3) &&
           (commandComponents[0] == "VLD") &&
           (commandComponents[1].size() == 5) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[1])) &&
           (commandComponents[2].size() == 4) &&
           (redesHelperFunctions::checkNumericOnlyString(commandComponents[2]));
}