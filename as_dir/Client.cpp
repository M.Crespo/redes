#include "Client.hpp"



bool redesClient::isLoggedIn(const std::string& UID){
    return redesHelperFunctions::fileExists("users/" + UID + "/" + UID+ "_login.txt");
}


void redesClient::writeTID(const std::string& UID, const std::string& TID, const std::string& FOP, const std::string& filename){
    std::string pathToFile = "users/"+UID+"/"+UID+"_tid.txt";
    std::string content = TID + "\n"+ FOP + "\n" + filename + "\n";

    redesHelperFunctions::writeToFile(pathToFile, content);
}

void redesClient::writeTID(const std::string& UID, const std::string& TID, const std::string& FOP){
    std::string pathToFile = "users/"+UID+"/"+UID+"_tid.txt";

    std::string content = TID + "\n"+ FOP + "\n";

    redesHelperFunctions::writeToFile(pathToFile, content);
}


std::string redesClient::getPDIP(const std::string& UID)
{
    std::string pdInfoFile = "users/"+UID+"/"+UID+"_reg.txt";

    if(redesHelperFunctions::fileExists(pdInfoFile)){
        std::vector<std::string> info;

        redesHelperFunctions::readFromFile(pdInfoFile, info);
        return info[0];
    }
    else{
        throw NoUserPDInfoException();
    }
}
std::string redesClient::getPDPort(const std::string& UID)
{
    std::string pdInfoFile = "users/"+UID+"/"+UID+"_reg.txt";

    if(redesHelperFunctions::fileExists(pdInfoFile)){
        std::vector<std::string> info;

        redesHelperFunctions::readFromFile(pdInfoFile, info);
        return info[1];
    }
    else{
        throw NoUserPDInfoException();
    }
}

bool redesClient::checkCredentials(const std::string& UID, const std::string& password){
    std::string passInfoFile = "users/"+UID+"/"+UID+"_pass.txt";
    if(redesHelperFunctions::fileExists(passInfoFile)){
        std::vector<std::string> info;
        redesHelperFunctions::readFromFile(passInfoFile, info);
        redesHelperFunctions::sanitizeString(info[0]);
        return password == info[0];
    }
    return false;
}

//Update User Info and PD Info into the respective files
void redesClient::update(const std::string& UID,const std::string& password, const std::string& PDIP, const std::string& PDPort){
    if(redesHelperFunctions::fileExists("users/" + UID + "/" + UID+ "_pass.txt") && !checkCredentials(UID, password)){
        throw InvalidRegCredentialsException();
    }
    updateUserInfo(UID, password);
    updatePDInfo(UID, PDIP, PDPort);
}

//Update PD Info into respective file
void redesClient::updatePDInfo(const std::string& UID, const std::string& PDIP, const std::string& PDPort){
    
    std::string newPDFileName = "users/" + UID + "/" + UID+ "_reg.txt";
    std::string fileContent =  PDIP + "\n" + PDPort + "\n";

    redesHelperFunctions::writeToFile(newPDFileName, fileContent);

}

//Update User Info into respective file
void redesClient::updateUserInfo(const std::string& UID, const std::string& password){
    std::string dirName = "users/" + UID;
    struct stat st = {0};
    if(stat(dirName.data(), &st) == -1){
        mkdir(dirName.data(), 0700);
    }
    std::string newUserFileName = dirName + "/" + UID+ "_pass.txt";
    std::string fileContent = password;

    redesHelperFunctions::writeToFile(newUserFileName, fileContent);
}

void redesClient::deletePDInfo(const std::string& UID){
    std::string fileName = "users/" + UID + "/"+ UID + "_reg.txt";
    redesHelperFunctions::deleteFile(fileName);
}

void redesClient::login(const std::string& UID, const std::string& password, const int fd){
    std::string dirName = "users/" + UID;
    std::string passwordFile = dirName + "/" + UID+ "_pass.txt";
    if(checkCredentials(UID, password)){
        std::string newLoginFileName = "users/" + UID + "/" + UID+ "_login.txt";
        std::string fileContent = std::to_string(fd) +"\n";

        redesHelperFunctions::writeToFile(newLoginFileName, fileContent);
        return;
    }
    throw redesClient::InvalidLOGCredentialsException();
}

void redesClient::logout(const std::string& UID){
    std::string dirName = "users/" + UID;
    std::string passwordFile = dirName + "/" + UID+ "_login.txt";
    if (redesHelperFunctions::fileExists(passwordFile))
        redesHelperFunctions::deleteFile(passwordFile);
}


std::pair<std::string, std::string> redesClient::getPDInfoFromFile(const std::string& UID){
    std::string dirName = "users/" + UID;
    std::string pdInfoFile = dirName + "/" + UID+ "_reg.txt";

    if(redesHelperFunctions::fileExists(pdInfoFile)){
        std::vector<std::string> info;
        redesHelperFunctions::readFromFile(pdInfoFile, info);
        return std::pair<std::string, std::string>(info[0], info[1]);
    }
    throw redesClient::NoUserPDInfoException();
}


