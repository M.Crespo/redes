#ifndef CHECKCOMMANDS_HPP
#define CHECKCOMMANDS_HPP
#include <iostream>

#include "../shared/helperFunctions.hpp"

bool checkREGCommand(const std::string &command);

bool checkUNRCommand(const std::string &command);

bool checkVLCCommand(const std::string &command);

bool checkRVCCommand(const std::string &command);

bool checkLOGCommand(const std::string &command);

bool checkREQCommand(const std::string &command);

bool checkAUTCommand(const std::string &command);

bool checkVLDCommand(const std::string &command);
#endif