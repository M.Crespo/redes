#ifndef CLIENT_CPP
#define CLIENT_CPP

#include <iostream>
#include <dirent.h>
#include <vector>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>

#include "../shared/helperFunctions.hpp"

namespace redesClient{

    std::string getPDIP(const std::string& UID);

    std::string getPDPort(const std::string& UID);

    bool checkCredentials(const std::string& UID, const std::string& password);

    void update(const std::string& UID,const std::string& password, const std::string& PDIP, const std::string& PDPort);

    void updatePDInfo(const std::string& UID, const std::string& PDIP, const std::string& PDPort);

    void updateUserInfo(const std::string& UID, const std::string& password);

    void deletePDInfo(const std::string& UID);

    bool isLoggedIn(const std::string& UID);
    
    void login(const std::string& UID, const std::string& password, const int fd);

    void logout(const std::string& UID);

    std::pair<std::string, std::string> getPDInfoFromFile(const std::string& UID);

    void writeTID(const std::string& UID, const std::string& TID, const std::string& FOP, const std::string& filename);
    void writeTID(const std::string& UID, const std::string& TID, const std::string& FOP);

    //Exceptions
    struct InvalidRegCredentialsException : public std::exception {
        const char* what() const throw() {
            return "InvalidRegCredentialsException Exception\n";
        }
    };

    struct NoSuchUserException : public std::exception {
        const char* what() const throw() {
            return "NoSuchUserException Exception\n";
        }
    };
    struct InvalidLOGCredentialsException : public std::exception {
        const char* what() const throw() {
            return "InvalidLOGCredentialsException Exception\n";
        }
    };

    struct NoUserPDInfoException : public std::exception {
        const char* what() const throw() {
            return "NoUserPDInfoException Exception\n";
        }
    };

}

#endif