#include "processCommands.hpp"

void processREGCommand(UDPServer *udpServer, const std::vector<std::string> &components, bool verbose)
{
    std::string UID = components[1];
    std::string password = components[2];
    std::string PDIp = components[3];
    std::string PDPort = components[4];
    try
    {
        redesClient::update(UID, password, PDIp, PDPort);
        udpServer->sendPayload("RRG OK\n");
        if (verbose)
        {
            auto addrInfo = udpServer->getAddressInfo();
            std::cout << "----- User Registration Successful -----" << std::endl;
            std::cout << "User ID: " << UID << std::endl;
            std::cout << "Password: " << password << std::endl;
            std::cout << "IP address: " << addrInfo.first << std::endl;
            std::cout << "Port Number: " << addrInfo.second << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
    }
    catch (redesClient::InvalidRegCredentialsException &you)
    {
        udpServer->sendPayload("RRG NOK\n");
        if (verbose)
        {
            auto addrInfo = udpServer->getAddressInfo();
            std::cout << "----- User Registration Failed -----" << std::endl;
            std::cout << "User ID: " << UID << std::endl;
            std::cout << "Password: " << password << std::endl;
            std::cout << "IP address: " << addrInfo.first << std::endl;
            std::cout << "Port Number: " << addrInfo.second << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
    }
}

void processUNRCommand(UDPServer *udpServer, const std::vector<std::string> &components, bool verbose)
{
    std::string UID = components[1];
    std::string password = components[2];

    try
    {
        redesClient::deletePDInfo(UID);
        udpServer->sendPayload("RUN OK\n");
        if (verbose)
        {
            auto addrInfo = udpServer->getAddressInfo();
            std::cout << "----- PD Unregistration Successful -----" << std::endl;
            std::cout << "User ID: " << UID << std::endl;
            std::cout << "Password: " << password << std::endl;
            std::cout << "IP address: " << addrInfo.first << std::endl;
            std::cout << "Port Number: " << addrInfo.second << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
    }
    catch (const std::out_of_range &e)
    {
        udpServer->sendPayload("RUN NOK\n");
        if (verbose)
        {
            auto addrInfo = udpServer->getAddressInfo();
            std::cout << "----- PD Unregistration Failed -----" << std::endl;
            std::cout << "IP address: " << addrInfo.first << std::endl;
            std::cout << "Port Number: " << addrInfo.second << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
    }
}

// void processRVCCommand(UDPServer *udpServer, const std::vector<std::string> &components, bool verbose)
// {
//     if (verbose)
//     {
//         auto addrInfo = udpServer->getAddressInfo();
//         std::cout << "----- PD 2-FA Validation Code OK-----" << std::endl;
//         std::cout << "IP address: " << addrInfo.first << std::endl;
//         std::cout << "Port Number: " << addrInfo.second << std::endl;
//         std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
//     }
// }

bool processLOGCommand(TCPServer *tcpServer, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose)
{
    try
    {
        redesClient::login(components[1], components[2], fd);
        if (verbose)
        {
            std::cout << "----- User Login Successful-----" << std::endl;
            std::cout << "UID: " << components[1] << std::endl;
            std::cout << "Password: " << components[2] << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
        tcpServer->sendPayload(fd, "RLO OK\n");
    }
    catch (redesClient::InvalidLOGCredentialsException &)
    {
        if (verbose)
        {
            std::cout << "----- User Login Failed-----" << std::endl;
            std::cout << "UID: " << components[1] << std::endl;
            std::cout << "Password: " << components[2] << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
        tcpServer->sendPayload(fd, "RLO NOK\n");
        return false;
    }
    return true;
}

redesHelperFunctions::request processREQCommand(TCPServer *tcpServer, UDPServer *udpServer, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose)
{
    std::string PDIP;
    std::string PDPort;
    std::string UID = components[1];
    std::string FOP = components[3];

    redesHelperFunctions::request returnRequest;
    returnRequest.UID = "";
    returnRequest.validationCode = "";
    returnRequest.requestID = "";
    returnRequest.FOP = "";
    returnRequest.filename = "";
    int validationCode = redesHelperFunctions::generateRandomNumber(1000, 10000);

    try
    {
        if (!redesClient::isLoggedIn(UID))
        {
            tcpServer->sendPayload(fd, "RRQ ELOG\n");
            if (verbose)
            {
                std::cout << "----- User Request Failed (ELOG) -----" << std::endl;
                std::cout << "UID: " << UID << std::endl;
                std::cout << "FOP: " << FOP << std::endl;
                std::cout << "IP: " << ip << std::endl;
                std::cout << "Port: " << port << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
            return returnRequest;
        }

        PDIP = redesClient::getPDIP(UID);
        PDPort = redesClient::getPDPort(UID);
    }
    catch (redesClient::NoUserPDInfoException &e)
    {
        tcpServer->sendPayload(fd, "RRQ EPD\n");
        if (verbose)
        {
            std::cout << "----- User Request Failed (EPD) -----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "FOP: " << FOP << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
        return returnRequest;
    }

    std::string messageToPD = "VLC " + UID + " " + std::to_string(validationCode) + " " + FOP;
    //if operation over specific file
    if (components.size() > 4)
    {
        messageToPD += " " + components[4];
    }

    messageToPD = messageToPD + "\n";
    std::string PDResponse;

    UDPSocket *sock = new UDPSocket(PDIP, PDPort, AF_INET, 128);

    sock->sendPayload(messageToPD);

    if (sock->readPayload(PDResponse) < 1)
    {
        tcpServer->sendPayload(fd, "RRQ EPD\n");
        if (verbose)
        {
            std::cout << "----- User Request Failed (EPD) -----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "FOP: " << FOP << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
        delete sock;
        return returnRequest;
    }
    delete sock;

    if (PDResponse.find("OK") != std::string::npos)
    {
        tcpServer->sendPayload(fd, "RRQ OK\n");
    }
    else
    {
        tcpServer->sendPayload("RRQ EUSER\n");
        if (verbose)
        {
            std::cout << "----- User Request Failed (EUSER) -----" << std::endl;
            std::cout << "UID: " << UID << std::endl;
            std::cout << "FOP: " << FOP << std::endl;
            std::cout << "IP: " << ip << std::endl;
            std::cout << "Port: " << port << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
        return returnRequest;
    }

    returnRequest.UID = UID;
    returnRequest.validationCode = std::to_string(validationCode);
    returnRequest.requestID = components[2];
    returnRequest.FOP = FOP;
    if (components.size() > 4)
    {
        returnRequest.filename = components[4];
    }
    if (verbose)
    {
        std::cout << "----- User Request Successful -----" << std::endl;
        std::cout << "UID: " << returnRequest.UID << std::endl;
        std::cout << "VC: " << returnRequest.validationCode << std::endl;
        std::cout << "RID: " << returnRequest.requestID << std::endl;
        std::cout << "FOP: " << returnRequest.FOP << std::endl;
        if (returnRequest.filename != "")
        {
            std::cout << "File Name: " << returnRequest.filename << std::endl;
        }
        std::cout << "IP: " << ip << std::endl;
        std::cout << "Port: " << port << std::endl;
        std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
    }
    return returnRequest;
}

void processAUTCommand(TCPServer *tcpServer, int fd, std::string ip, std::string port, const std::vector<std::string> &components, bool verbose, std::vector<redesHelperFunctions::request> &requests)
{
    std::string UID = components[1];
    std::string RID = components[2];
    std::string VCD = components[3];

    for (redesHelperFunctions::request req : requests)
    {
        if (req.requestID == RID && req.UID == UID)
        {
            if (req.validationCode == VCD)
            {
                std::string TID = std::to_string(redesHelperFunctions::generateRandomNumber(1000, 10000));
                tcpServer->sendPayload(fd, "RAU " + TID + "\n");
                if (req.FOP == "L" || req.FOP == "X")
                    redesClient::writeTID(UID, TID, req.FOP);
                else
                    redesClient::writeTID(UID, TID, req.FOP, req.filename);
                return;
                if (verbose)
                {
                    std::cout << "----- Authentication Request Successful -----" << std::endl;
                    std::cout << "UID: " << UID << std::endl;
                    std::cout << "RID: " << RID << std::endl;
                    std::cout << "VCD: " << VCD << std::endl;
                    std::cout << "IP: " << ip << std::endl;
                    std::cout << "Port: " << port << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                }
            }
        }
    }
    tcpServer->sendPayload(fd, "RAU 0\n");
    if (verbose)
    {
        std::cout << "----- Authentication Request Failed -----" << std::endl;
        std::cout << "UID: " << UID << std::endl;
        std::cout << "RID: " << RID << std::endl;
        std::cout << "VCD: " << VCD << std::endl;
        std::cout << "IP: " << ip << std::endl;
        std::cout << "Port: " << port << std::endl;
        std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
    }
}

void processVLDCommand(UDPServer *server, const std::vector<std::string> &components, bool verbose)
{
    std::string UID = components[1];
    std::string TID = components[2];

    std::string TIDUserFileName = "users/" + UID + "/" + UID + "_tid.txt";

    std::vector<std::string> info;
    redesHelperFunctions::readFromFile(TIDUserFileName, info);

    if (info.size() == 0)
    {
        std::string payload = "CNF " + UID + " " + TID + " " + "E" + "\n";
    }

    std::string fileTID = info[0];
    std::string fileOP = info[1];
    std::string fileName = "";
    if (info.size() == 3)
    {
        fileName = info[2];
    }

    if (TID != fileTID)
    {
        fileOP = "E";
    }

    if (fileOP == "X")
    {
        redesHelperFunctions::deleteDirectory("users/" + UID + "/");
    }

    std::string payload = "CNF " + UID + " " + TID + " " + fileOP + " " + fileName + "\n";

    server->sendPayload(payload);
    auto addrInfo = server->getAddressInfo();
    if (verbose && fileOP != "E")
    {
        std::cout << "----- FS Validation Request Successful-----" << std::endl;
        std::cout << "UID: " << UID << std::endl;
        std::cout << "TID: " << TID << std::endl;
        std::cout << "FOP: " << fileOP << std::endl;
        if (fileName != "")
        {
            std::cout << "File Name: " << fileName << std::endl;
        }
        std::cout << "IP: " << addrInfo.first << std::endl;
        std::cout << "Port: " << addrInfo.second << std::endl;
        std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
    }
    else
    {
        std::cout << "----- FS Validation Request Failed -----" << std::endl;
        std::cout << "UID: " << UID << std::endl;
        std::cout << "TID: " << TID << std::endl;
        std::cout << "FOP: " << fileOP << std::endl;
        if (fileName != "")
        {
            std::cout << "File Name: " << fileName << std::endl;
        }
        std::cout << "IP: " << addrInfo.first << std::endl;
        std::cout << "Port: " << addrInfo.second << std::endl;
        std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
    }
    redesHelperFunctions::deleteFile("users/"+UID+"/"+UID+"_tid.txt");
    return;
}