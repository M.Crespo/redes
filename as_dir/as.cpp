
#include <iostream>
#include "../shared/TCPServer.hpp"
#include "../shared/UDPServer.hpp"
#include "../shared/helperFunctions.hpp"
#include <vector>
#include <algorithm>
#include <csignal>
#include <sys/ioctl.h>
#include <map>
#include "Client.hpp"
#include <time.h>
#include "checkCommands.hpp"
#include "processCommands.hpp"

#define MAX(a, b) (a > b ? a : b)

std::string ASPort = "58062";
std::string ASIP = "127.0.0.1";

bool verbose;

std::map<int, redesHelperFunctions::fdInfo> fdMap;

std::vector<redesHelperFunctions::request> requests;

void checkActiveClients()
{
    //skip UDP and TCP Servers
    for (auto it = fdMap.begin(); it != fdMap.end();)
    {
        if (it->second.isActive)
        {
            it->second.isActive = false;
            continue;
        }
        else
        {
            std::string UID = it->second.UID;
            redesHelperFunctions::deleteFile("users/"+UID+"/"+UID+"_login.txt");
            close(it->first);
            fdMap.erase(it++);
            continue;
        }
        it++;
    }
}

bool isValidFop(const std::string& fop){
    return (fop == "L" || fop == "X" ||  fop == "R" || fop == "U" || fop == "D");

}

void showUsage()
{
    std::cout << "./AS [-p ASPort] [-v]" << std::endl;
}

bool processInput(int argc, char **argv)
{
    if (argc > 4)
    {
        return false;
    }

    if (argc == 1)
    {
        return true;
    }

    for (int i = 1; i < argc; i++)
    {
        if (std::string(argv[i]) == "-p")
        {
            if (argc < 3)
            {
                return false;
            }
            if (std::string(argv[i + 1]).find_first_not_of("[0123456789]") != std::string::npos)
            {
                return false;
            }
            ASPort = argv[i + 1];
        }
        else if (std::string(argv[i]) == "-v")
        {
            verbose = true;
        }
    }
    return true;
}


void parseUDPMessage(UDPServer *udpServer)
{

    std::string message;

    udpServer->readPayload(message);
    auto components = redesHelperFunctions::getCommandComponents(message);
    std::string commandID = components[0];
    if (commandID == "REG")
    {
        if (checkREGCommand(message))
        {
            processREGCommand(udpServer, components, verbose);
            
        }
        else
        {
            if(verbose){
                auto addrInfo = udpServer->getAddressInfo();
                std::cout << "----- Wrong Protocol Used (REG) -----" << std::endl;
                std::cout << "IP address: " << addrInfo.first << std::endl;
                std::cout << "Port Number: " << addrInfo.second << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }

            udpServer->sendPayload("ERR\n");
        }
    }
    else if (commandID == "UNR")
    {
        if (checkUNRCommand(message))
        {
            processUNRCommand(udpServer, components, verbose);
        }
        else
        {
            if(verbose){
                auto addrInfo = udpServer->getAddressInfo();
                std::cout << "----- Wrong Protocol Used (UNR) -----" << std::endl;
                std::cout << "IP address: " << addrInfo.first << std::endl;
                std::cout << "Port Number: " << addrInfo.second << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
            udpServer->sendPayload("ERR\n");
        }
    }
    // else if (commandID == "RVC")
    // {
    //     if (checkRVCCommand(message))
    //     {
    //         processRVCCommand(udpServer, components, verbose);
    //     }
    //     else
    //     {
    //         if(verbose){
    //             auto addrInfo = udpServer->getAddressInfo();
    //             std::cout << "----- Wrong Protocol Used (RVC) -----" << std::endl;
    //             std::cout << "IP address: " << addrInfo.first << std::endl;
    //             std::cout << "Port Number: " << addrInfo.second << std::endl;
    //             std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
    //         }
    //         udpServer->sendPayload("ERR\n");
    //     }
    // }
    else if (commandID == "VLD")
    {
        if (checkVLDCommand(message))
        {
            processVLDCommand(udpServer, components, verbose);
        }
        else
        {
            if(verbose){
                auto addrInfo = udpServer->getAddressInfo();
                std::cout << "----- Wrong Protocol Used (RVC) -----" << std::endl;
                std::cout << "IP address: " << addrInfo.first << std::endl;
                std::cout << "Port Number: " << addrInfo.second << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
            udpServer->sendPayload("ERR\n");
        }
    }
    else
    {
        if(verbose){
            auto addrInfo = udpServer->getAddressInfo();
            std::cout << "----- Wrong Protocol Used (Command not defined) -----" << std::endl;
            std::cout << "IP address: " << addrInfo.first << std::endl;
            std::cout << "Port Number: " << addrInfo.second << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
        udpServer->sendPayload("ERR\n");
    }
}

int parseTCPMessage(TCPServer *tcpServer, UDPServer* udpServer,  int fd)
{
    std::string message;

    int n = tcpServer->readPayload(fd, message);
    std::string tcpIp = fdMap.at(fd).ip;
    std::string tcpPort = fdMap.at(fd).port;

    auto components = redesHelperFunctions::getCommandComponents(message);
    std::string commandID = components[0];

    if(commandID == "LOG"){
        if (checkLOGCommand(message))
        {
            if (processLOGCommand(tcpServer, fd, tcpIp, tcpPort, components, verbose)){
                std::string UID = components[1];
                fdMap.at(fd).UID = UID;
            }
        }
        else
        {   
            if(verbose){
                std::cout << "----- Wrong Protocol Used (LOG) -----" << std::endl;
                std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
            }
            tcpServer->sendPayload(fd, "ERR\n");
        }
    }
    else if(commandID == "REQ"){
        if (checkREQCommand(message))
        {
            redesHelperFunctions::request req = processREQCommand(tcpServer, udpServer, fd, tcpIp, tcpPort, components, verbose);
            if(req.UID != ""){
                
                requests.push_back(req);
            }

        }
        else
        {
            std::vector<std::string> vec = redesHelperFunctions::getCommandComponents(message);
            if(!isValidFop(vec[3])){
                tcpServer->sendPayload(fd, "RRQ EFOP\n");
                if(verbose){
                    std::cout << "----- User Request Failed: Invalid FOP----" << std::endl;
                    std::cout << "FOP: " << vec[3] << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                }
            }
            else{
                tcpServer->sendPayload(fd, "RRQ ERR\n");
                if(verbose){
                    std::cout << "----- User Request Failed: Wront REQ Protocol----" << std::endl;
                    std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
                }
            }
        }
    }
    else if(commandID == "AUT"){
        if(checkAUTCommand(message)){
            processAUTCommand(tcpServer, fd, tcpIp, tcpPort, components, verbose, requests);
        }
    }
    else if (n == 0){
        //TCP connection closed
        return n;
    }
    else{
        if(verbose){
            std::cout << "----- Wrong Protocol Used (ERR) -----" << std::endl;
            std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
        }
        tcpServer->sendPayload(fd, "ERR\n");
    }

    return n;
}

int main(int argc, char **argv)
{
    fd_set rfds;
    int biggestFD;
    verbose = false;
    if (!processInput(argc, argv))
    {
        showUsage();
        exit(EXIT_FAILURE);
    }

    auto udpServer = new UDPServer(ASPort, 128);
    auto tcpServer = new TCPServer(ASPort, 128);
    
    biggestFD = tcpServer->getFileDescriptor();

    while (1)
    {
        FD_ZERO(&rfds);
        struct timeval tv = {20, 0};
        //Set the fds again
        FD_SET(tcpServer->getFileDescriptor(), &rfds);
        FD_SET(udpServer->getFileDescriptor(), &rfds);
        for (auto it = fdMap.begin(); it != fdMap.end(); it++)
        {
            FD_SET(it->first, &rfds);
        }

        int selectReturn = select(biggestFD + 1, &rfds, nullptr, nullptr, &tv);

        if (selectReturn == -1)
        {
            perror("Error on pselect.");
            exit(EXIT_FAILURE);
        }
        else if (selectReturn == 0)
        {
            checkActiveClients();
        }
        else if (selectReturn)
        {
            try
            {
                //New Connection
                if (FD_ISSET(tcpServer->getFileDescriptor(), &rfds))
                {
                    redesHelperFunctions::fdInfo fdInfo;
                    int fd = tcpServer->tcpAccept(fdInfo.ip, fdInfo.port);
                    fdMap.insert({fd, fdInfo});
                    biggestFD = MAX(biggestFD, fd);
                }
                //New UDP Message
                if (FD_ISSET(udpServer->getFileDescriptor(), &rfds))
                {
                    parseUDPMessage(udpServer);
                }
                //New TCP message
                for (auto it = fdMap.begin(); it != fdMap.end();)
                {
                    if (FD_ISSET(it->first, &rfds))
                    {
                        if (parseTCPMessage(tcpServer, udpServer, it->first))
                        {
                            it->second.isActive = true;
                        }
                        else
                        {
                            close(it->first);
                            redesClient::logout(it->second.UID);
                            fdMap.erase(it++);
                            continue;
                        }
                    }
                    it++;
                }
            }
            catch (const redesHelperFunctions::ExitSucessException &)
            {
                //Break out of the infinite loop of server coms, so we can orderly exit the server

                //Maybe do the same for exit failure?
                break;
            }
        }
    }

    delete tcpServer;
    delete udpServer;
    return 0;
}
