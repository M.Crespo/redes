SOURCES = user_dir/user.cpp
SOURCES += shared/TCPSocket.cpp 
SOURCES += shared/UDPSocket.cpp 
SOURCES += pd_dir/pd.cpp
SOURCES += as_dir/as.cpp 
SOURCES += fs_dir/fs.cpp 
SOURCES += fs_dir/checkCommands.cpp
SOURCES += fs_dir/processCommands.cpp
SOURCES += pd_dir/pdAPI.cpp
SOURCES += user_dir/userAPI.cpp
SOURCES += shared/UDPServer.cpp
SOURCES += shared/TCPServer.cpp
SOURCES += shared/helperFunctions.cpp
SOURCES += as_dir/Client.cpp
SOURCES += as_dir/checkCommands.cpp
SOURCES += as_dir/processCommands.cpp
OBJS = $(SOURCES:%.cpp=%.o)
CC   = g++
LD   = g++
CFLAGS =-std=c++14 -Wall -lm
TARGETS = user pd AS FS
.PHONY: all clean 

all: $(TARGETS)

$(TARGETS):
	$(LD) $(CFLAGS) $^ -o $@ $(LDFLAGS)


shared/helperFunctions.o: shared/helperFunctions.cpp
shared/TCPSocket.o: shared/TCPSocket.cpp shared/AbstractSocket.hpp
shared/UDPSocket.o: shared/UDPSocket.cpp shared/AbstractSocket.hpp
shared/UDPServer.o: shared/UDPServer.cpp shared/UDPSocket.o
shared/TCPServer.o: shared/TCPServer.cpp shared/TCPSocket.o

as_dir/Client.o: as_dir/Client.cpp shared/helperFunctions.o 
as_dir/checkCommands.o: as_dir/checkCommands.cpp shared/helperFunctions.o 
as_dir/processCommands.o: as_dir/processCommands.cpp shared/helperFunctions.o as_dir/Client.o
fs_dir/checkCommands.o: fs_dir/checkCommands.cpp shared/helperFunctions.o 
fs_dir/processCommands.o: fs_dir/processCommands.cpp shared/helperFunctions.o

pd_dir/pdAPI.o: pd_dir/pdAPI.cpp shared/UDPSocket.o
user_dir/userAPI.o: user_dir/userAPI.cpp shared/TCPSocket.o shared/helperFunctions.o


pd_dir/pd.o: pd_dir/pd.cpp shared/UDPSocket.o
user_dir/user.o: user_dir/user.cpp user_dir/userAPI.o shared/TCPSocket.o shared/UDPSocket.o shared/helperFunctions.o
fs_dir/fs.o: fs_dir/fs.cpp
as_dir/as.o: as_dir/as.cpp

FS: fs_dir/fs.o fs_dir/checkCommands.o fs_dir/processCommands.o shared/helperFunctions.o shared/UDPSocket.o shared/UDPServer.o shared/TCPSocket.o shared/TCPServer.o
AS: as_dir/as.o as_dir/Client.o as_dir/checkCommands.o as_dir/processCommands.o shared/UDPSocket.o shared/UDPServer.o shared/TCPSocket.o shared/TCPServer.o shared/helperFunctions.o 
pd: pd_dir/pd.o shared/UDPSocket.o shared/UDPServer.o pd_dir/pdAPI.o shared/helperFunctions.o
user: user_dir/user.o shared/TCPSocket.o  user_dir/userAPI.o shared/helperFunctions.o

%.o:
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	@echo Cleaning...
	rm -f $(OBJS) $(TARGETS)
