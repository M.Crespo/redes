# Redes

Projecto Redes

## Building 

You can build the project by running make.

### Uploading user files

All files that a user wants to upload to the FS must be inside a folder called <b>upload_files<b>, so this directory must
be created.

### Default AS and FS IP Addresses

The default IP adresses for the AS and FS are 193.136.128.103 (sigma03). 

To change the default IP run for both AS and FS:

```
sh
./configure.sh <Old_IP> <New_IP>
```

To change individually:
```
sh
./configure.sh <Old_AS_IP> <New_AS_IP> <Old_FS_IP> <New_FS_IP>
```
